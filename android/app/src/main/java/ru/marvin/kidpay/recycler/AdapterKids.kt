package ru.marvin.kidpay.recycler

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import ru.marvin.kidpay.R
import ru.marvin.kidpay.model.Kid

class AdapterKids(private val kids: List<Kid>, private val kidClickListener: ((kid: Kid) -> Unit)) :
    RecyclerView.Adapter<ViewHolderKid>() {

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ViewHolderKid {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycler_kid, parent, false)
        return ViewHolderKid(view)
    }

    override fun getItemCount(): Int {
        return kids.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolderKid, position: Int) {
        val kid = kids[position]
        viewHolder.fill(kid)
        viewHolder.itemView.setOnClickListener { kidClickListener.invoke(kid) }
    }
}