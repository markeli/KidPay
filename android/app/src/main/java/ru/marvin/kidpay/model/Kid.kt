package ru.marvin.kidpay.model

import com.google.gson.JsonObject

class Kid(json: JsonObject) {
    val id: Int = json.get("id").asInt
    val fullName: String = json.get("name").asString
    val phoneNumber: String = json.get("phone").asString
}