package ru.marvin.kidpay.model

import com.google.gson.JsonObject

class Limit(json: JsonObject) {
    val id: Int = json.get("id").asInt
    val childId: Int = json.get("childId").asInt
    val categoryId: Int = json.get("categoryId").asInt
    val limit: Int = json.get("limit").asInt
    val currentAmount: Int = json.get("currentAmount").asInt
}