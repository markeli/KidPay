package ru.marvin.kidpay.recycler

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar
import ru.marvin.kidpay.R

class ViewHolderLimit(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val textViewCategoryName: TextView = itemView.findViewById(R.id.textViewCategoryName)
    private val progressBar: RoundCornerProgressBar = itemView.findViewById(R.id.progressBar)
    private val textViewLimit: TextView = itemView.findViewById(R.id.textViewLimit)

    fun fill(category: String, limit: Int, currentAmount: Int) {
        textViewCategoryName.text = category
        textViewLimit.text = "$limit€"
        progressBar.max = limit.toFloat()
        progressBar.progress = currentAmount.toFloat()
        progressBar.postInvalidate()
    }
}