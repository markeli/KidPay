package ru.marvin.kidpay.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.text.InputType
import com.afollestad.materialdialogs.MaterialDialog
import kotlinx.android.synthetic.main.activity_kid_limits.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.marvin.kidpay.R
import ru.marvin.kidpay.model.Category
import ru.marvin.kidpay.model.Limit
import ru.marvin.kidpay.network.RequestRepository
import ru.marvin.kidpay.recycler.AdapterKids
import ru.marvin.kidpay.recycler.AdapterLimits
import org.greenrobot.eventbus.ThreadMode
import org.greenrobot.eventbus.Subscribe
import ru.marvin.kidpay.messaging.ChildSpentEvent
import org.greenrobot.eventbus.EventBus




class KidLimitsActivity : AppCompatActivity() {
    private val kidId: Int by lazy { intent.getIntExtra("ru.marvin.kidpay.kid_id", -1) }
    private val kidName: String by lazy { intent.getStringExtra("ru.marvin.kidpay.kid_name") }
    private val kidPhone: String by lazy { intent.getStringExtra("ru.marvin.kidpay.kid_phone") }

    private var categories = ArrayList<Category>()
    private var limits = ArrayList<Limit>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kid_limits)

        textViewKidName.text = kidName
        textViewKidPhone.text = kidPhone

        loadLimitsAndCategories()

        buttonAddCategory.setOnClickListener { addCategory() }
        textAddCategory.setOnClickListener { addCategory() }

        val layoutManager = LinearLayoutManager(this)
        val divider = DividerItemDecoration(this, layoutManager.orientation)

        recyclerLimits.layoutManager = layoutManager
        recyclerLimits.addItemDecoration(divider)
        recyclerLimits.adapter = AdapterLimits(emptyList(), emptyList()) { limit, category -> onLimitClick(limit, category)}
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: ChildSpentEvent) {
        loadLimitsAndCategories()
    }

    public override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    public override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    private fun loadLimitsAndCategories() {
        GlobalScope.launch(Dispatchers.Main) {
            val newCategories = RequestRepository.getAvailableCategories()
            val newLimits = RequestRepository.getChildLimits(kidId)

            categories.clear()
            categories.addAll(newCategories)

            limits.clear()
            limits.addAll(newLimits)

            recyclerLimits.adapter = AdapterLimits(categories, limits) {limit, category -> onLimitClick(limit, category)}
        }
    }

    private fun addCategory() {
        val setCategoriesIds = limits.map { limit -> limit.categoryId }
        val notSetCategories = categories.filter { category -> category.id !in setCategoriesIds }

        MaterialDialog.Builder(this)
            .title("Select a category")
            .items(notSetCategories.map { category -> category.title })
            .itemsCallbackSingleChoice(-1) { dialog, _, selected, _ ->
                if (selected != -1) {
                    val selectedCategory = notSetCategories[selected]
                    MaterialDialog.Builder(this)
                        .title("Enter monthly limit on category \"${selectedCategory.title}\"")
                        .inputType(InputType.TYPE_CLASS_NUMBER)
                        .input("Limit in euro", null, false) { innerDialog, result ->
                            addLimit(selectedCategory, result.toString().toInt())
                            innerDialog.dismiss()
                        }
                        .positiveColorRes(R.color.colorPrimary)
                        .titleColorRes(R.color.colorAccent)
                        .backgroundColorRes(R.color.background)
                        .show()
                }
                dialog.dismiss()
                return@itemsCallbackSingleChoice true
            }
            .positiveText("Select")
            .itemsColorRes(R.color.colorAccent)
            .backgroundColorRes(R.color.background)
            .positiveColorRes(R.color.colorPrimary)
            .titleColorRes(R.color.colorAccent)
            .show()
    }

    private fun addLimit(selectedCategory: Category, selectedLimit: Int) {
        GlobalScope.launch(Dispatchers.Main) {
            RequestRepository.addChildLimit(kidId, selectedCategory.id, selectedLimit)
            loadLimitsAndCategories()
        }
    }

    private fun updateLimit(limit: Limit, newLimit: Int) {
        GlobalScope.launch(Dispatchers.Main) {
            RequestRepository.setChildLimit(kidId, limit.id, newLimit)
            loadLimitsAndCategories()
        }
    }

    private fun removeLimit(limit: Limit) {
        GlobalScope.launch(Dispatchers.Main) {
            RequestRepository.deleteChildLimit(kidId, limit.id)
            loadLimitsAndCategories()
        }
    }

    private fun onLimitClick(limit: Limit, category: Category) {
        MaterialDialog.Builder(this)
            .title("Edit monthly limit or remove category \"${category.title}\" from while-list")
            .inputType(InputType.TYPE_CLASS_NUMBER)
            .input("Limit in euro", limit.limit.toString(), false) { innerDialog, result ->
                updateLimit(limit, result.toString().toInt())
                innerDialog.dismiss()
            }
            .negativeText("Remove")
            .titleColorRes(R.color.colorAccent)
            .negativeColorRes(R.color.colorAccent)
            .positiveColorRes(R.color.colorPrimary)
            .backgroundColorRes(R.color.background)
            .onNegative { dialog, which -> removeLimit(limit); dialog.dismiss() }
            .show()
    }
}
