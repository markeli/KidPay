package ru.marvin.kidpay.ui

import android.app.*
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.GestureDetector
import android.view.MotionEvent
import com.mikepenz.materialdrawer.AccountHeaderBuilder
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mikepenz.materialdrawer.model.ProfileDrawerItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import ru.marvin.kidpay.R
import ru.marvin.kidpay.messaging.ChildSpentEvent
import ru.marvin.kidpay.network.RequestRepository


class MainPageActivity : AppCompatActivity(), GestureDetector.OnGestureListener {
    private val SWIPE_MIN_DISTANCE = 120
    private val SWIPE_MAX_OFF_PATH = 250
    private val SWIPE_THRESHOLD_VELOCITY = 200
    private var gestureDetector: GestureDetector? = null
    private var drawer: Drawer? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_page)
        gestureDetector = GestureDetector(this, this)
        drawer = DrawerBuilder()
            .withActivity(this)
            .withSliderBackgroundColorRes(R.color.colorAccent)
            .withAccountHeader(
                AccountHeaderBuilder()
                    .withActivity(this)
                    .withHeaderBackground(R.color.colorAccent)
                    .addProfiles(
                        ProfileDrawerItem()
                            .withName("Max Markelov")
                            .withIcon(R.mipmap.ic_user)
                    )
                    .withDividerBelowHeader(false)
                    .withSelectionListEnabled(false)
                    .build()
            )
            .addDrawerItems(
                PrimaryDrawerItem().withIdentifier(1).withName("Send, request & pay"),
                PrimaryDrawerItem().withIdentifier(2).withName("Activities"),
                PrimaryDrawerItem().withIdentifier(3).withName("Subscriptions"),
                PrimaryDrawerItem().withIdentifier(4).withName("KidPay"),
                PrimaryDrawerItem().withIdentifier(5).withName("Box"),
                PrimaryDrawerItem().withIdentifier(6).withName("Receipts"),
                PrimaryDrawerItem().withIdentifier(7).withName("Bonus"),
                PrimaryDrawerItem().withIdentifier(8).withName("Cards"),
                PrimaryDrawerItem().withIdentifier(9).withName("News")

            )
            .withSelectedItem(1)
            .withOnDrawerItemClickListener { _, position, _ ->
                if (position == 4) {
                    startActivity(Intent(this, KidsListActivity::class.java))
                    return@withOnDrawerItemClickListener true
                }
                return@withOnDrawerItemClickListener false
            }
            .build()

        createNotificationChannel()
        startNotificationCheck()
    }

    override fun onResume() {
        super.onResume()
        if (drawer?.isDrawerOpen == true) {
            drawer?.closeDrawer()
            drawer?.setSelection(1)
        }
    }

    private fun startNotificationCheck() {
        GlobalScope.launch(Dispatchers.Main) {
            while (true) {
                RequestRepository.getNotification()?.let {
                    val title = it.get("title").asString
                    val body = it.get("body").asString
                    val kidId = it.get("childId").asInt
                    val kidName = it.get("name").asString
                    val kidPhone = it.get("phone").asString

                    val intent = Intent(this@MainPageActivity, KidLimitsActivity::class.java)
                    intent.putExtra("ru.marvin.kidpay.kid_id", kidId)
                    intent.putExtra("ru.marvin.kidpay.kid_name", kidName)
                    intent.putExtra("ru.marvin.kidpay.kid_phone", kidPhone)

                    val resultPendingIntent: PendingIntent? = TaskStackBuilder.create(this@MainPageActivity).run {
                        addNextIntentWithParentStack(intent)
                        getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                    }


                    val notification = buildNotification(title, body, resultPendingIntent)
                    val notificationManager = getSystemService(Service.NOTIFICATION_SERVICE) as NotificationManager
                    notificationManager.notify(NOTIFICATION_ID, notification)
                    EventBus.getDefault().post(ChildSpentEvent())
                }
                delay(1000)
            }
        }
    }

    private fun createNotificationChannel() {
        // create android channel

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val androidChannel = NotificationChannel(
                NOTIFICATION_CHANNEL,
                "MobilePay notification channel",
                NotificationManager.IMPORTANCE_HIGH
            )
            androidChannel.enableLights(true)
            androidChannel.enableVibration(true)
            androidChannel.lightColor = Color.BLUE
            androidChannel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE

            val notificationManager = getSystemService(Service.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(androidChannel)
        }
    }

    private fun buildNotification(title: String, text: String, intent: PendingIntent?): Notification {
        return NotificationCompat.Builder(this, NOTIFICATION_CHANNEL)
            .setSmallIcon(R.mipmap.ic_launcher_foreground)
            .setColor(ContextCompat.getColor(this, R.color.colorAccent))
            .setContentTitle(title)
            .setStyle(NotificationCompat.BigTextStyle().bigText(text))
            .setDefaults(NotificationCompat.DEFAULT_SOUND)
            .setAutoCancel(true)
            .apply { if (intent != null) setContentIntent(intent) }
            .build()
    }

    override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {

        // Check movement along the Y-axis. If it exceeds SWIPE_MAX_OFF_PATH,
        // then dismiss the swipe.
        if (Math.abs(e1.y - e2.y) > SWIPE_MAX_OFF_PATH)
            return false

        // Swipe from left to right.
        // The swipe needs to exceed a certain distance (SWIPE_MIN_DISTANCE)
        // and a certain velocity (SWIPE_THRESHOLD_VELOCITY).
        if (e2.x - e1.x > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
            drawer?.openDrawer()
            return true
        }

        return false
    }

    override fun onShowPress(e: MotionEvent?) = Unit
    override fun onSingleTapUp(e: MotionEvent?): Boolean = false
    override fun onDown(e: MotionEvent?): Boolean = false
    override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean = false
    override fun onLongPress(e: MotionEvent?) = Unit

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        // TouchEvent dispatcher.
        if (gestureDetector != null) {
            if (gestureDetector?.onTouchEvent(ev) == true)
            // If the gestureDetector handles the event, a swipe has been
            // executed and no more needs to be done.
                return true
        }
        return super.dispatchTouchEvent(ev)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return gestureDetector?.onTouchEvent(event) == true
    }

    companion object {
        private const val NOTIFICATION_CHANNEL = "ru.marvin.kidpay.messagechannel"
        private const val NOTIFICATION_ID = 375
    }
}