package ru.marvin.kidpay.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*
import ru.marvin.kidpay.R

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        buttonLogin.setOnClickListener { login() }
    }

    private fun login() {
        val intent = Intent(this, MainPageActivity::class.java)

        startActivity(intent)
        finish()
    }
}
