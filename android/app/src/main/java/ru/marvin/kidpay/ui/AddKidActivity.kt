package ru.marvin.kidpay.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_add_kid.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.marvin.kidpay.R
import ru.marvin.kidpay.network.RequestRepository

class AddKidActivity : AppCompatActivity() {
    enum class CurrentStatus {
        None,
        PhoneSubmitted,
        PhoneConfirmed,
    }

    var currentStatus = CurrentStatus.None

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_kid)

        buttonSubmit.setOnClickListener { submit() }
    }

    private fun submit() {
        when (currentStatus) {
            CurrentStatus.None -> {
                currentStatus = CurrentStatus.PhoneSubmitted
                editTextPhone.isEnabled = false
                editTextSmsCode.visibility = View.VISIBLE
            }
            CurrentStatus.PhoneSubmitted -> {
                currentStatus = CurrentStatus.PhoneConfirmed
                editTextSmsCode.visibility = View.GONE
                editTextName.visibility = View.VISIBLE
                editTextPassword.visibility = View.VISIBLE
                editTextPasswordConfirm.visibility = View.VISIBLE
            }
            CurrentStatus.PhoneConfirmed -> {
                val phone = editTextPhone.text.toString()
                val name = editTextName.text.toString()
                val password = editTextPassword.text.toString()

                GlobalScope.launch(Dispatchers.Main) {
                    RequestRepository.addKid(phone, name)
                    finish()
                }
            }
        }
    }
}
