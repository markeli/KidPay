package ru.marvin.kidpay.recycler

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import ru.marvin.kidpay.R
import ru.marvin.kidpay.model.Kid

class ViewHolderKid(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val textViewKidName: TextView = itemView.findViewById(R.id.textViewKidName)
    val textViewKidPhone: TextView = itemView.findViewById(R.id.textViewKidPhone)

    fun fill(kid: Kid) {
        textViewKidName.text = kid.fullName
        textViewKidPhone.text = kid.phoneNumber
    }
}