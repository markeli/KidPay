package ru.marvin.kidpay.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_kids_list.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.marvin.kidpay.R
import ru.marvin.kidpay.model.Kid
import ru.marvin.kidpay.network.RequestRepository
import ru.marvin.kidpay.recycler.AdapterKids

class KidsListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kids_list)

        buttonAddKid.setOnClickListener { addNewKid() }
        textAddKid.setOnClickListener { addNewKid() }

        val layoutManager = LinearLayoutManager(this)
        val divider = DividerItemDecoration(this, layoutManager.orientation)

        recyclerViewKids.layoutManager = layoutManager
        recyclerViewKids.addItemDecoration(divider)
        recyclerViewKids.adapter = AdapterKids(emptyList()) { kidId -> showKidLimits(kidId) }
        refreshKidsList()
    }

    override fun onResume() {
        super.onResume()
        refreshKidsList()
    }

    private fun refreshKidsList() {
        GlobalScope.launch(Dispatchers.Main) {
            val kids = RequestRepository.getKids()
            recyclerViewKids.adapter = AdapterKids(kids) { kid -> showKidLimits(kid) }
        }
    }

    private fun addNewKid() {
        startActivity(Intent(this, AddKidActivity::class.java))
    }

    private fun showKidLimits(kid: Kid) {
        val intent = Intent(this, KidLimitsActivity::class.java)
        intent.putExtra("ru.marvin.kidpay.kid_id", kid.id)
        intent.putExtra("ru.marvin.kidpay.kid_name", kid.fullName)
        intent.putExtra("ru.marvin.kidpay.kid_phone", kid.phoneNumber)
        startActivity(intent)
    }
}
