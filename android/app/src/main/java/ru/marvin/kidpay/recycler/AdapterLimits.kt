package ru.marvin.kidpay.recycler

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import ru.marvin.kidpay.R
import ru.marvin.kidpay.model.Category
import ru.marvin.kidpay.model.Limit

class AdapterLimits(
    private val categories: List<Category>,
    private val limits: List<Limit>,
    private val limitClickListener: ((Limit, Category) -> Unit)
) :
    RecyclerView.Adapter<ViewHolderLimit>() {

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ViewHolderLimit {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycler_limit, parent, false)
        return ViewHolderLimit(view)
    }

    override fun getItemCount(): Int {
        return limits.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolderLimit, position: Int) {
        val limit = limits[position]
        val category = categories.first { limit.categoryId == it.id }
        viewHolder.fill(category.title, limit.limit, limit.currentAmount)
        viewHolder.itemView.setOnClickListener { limitClickListener.invoke(limit, category)}
    }
}