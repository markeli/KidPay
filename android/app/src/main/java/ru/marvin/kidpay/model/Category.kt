package ru.marvin.kidpay.model

import com.google.gson.JsonObject

class Category(json: JsonObject) {
    val id: Int = json.get("id").asInt
    val title: String = json.get("name").asString
}