package ru.marvin.kidpay.network

import com.github.kittinunf.fuel.Fuel
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.marvin.kidpay.model.Category
import ru.marvin.kidpay.model.Kid
import ru.marvin.kidpay.model.Limit


object RequestRepository {
    private val baseUrl = "http://207.180.236.115:5678/api"

    private val urlGetAvailableCategories = "$baseUrl/Categories"
    private val urlGetUserKids = "$baseUrl/Children"
    private val urlRegisterKid = "$baseUrl/Children"
    private val urlGetKidLimits = "$baseUrl/Children/%1\$d/limits"
    private val urlSetKidLimits = "$baseUrl/Children/%1\$d/limits"
    private val urlAddKidLimits = "$baseUrl/Children/%1\$d/limits"
    private val urlDeleteKidLimits = "$baseUrl/Children/%1\$d/limits/%2\$d"
    private val getNotification = "$baseUrl/Notifications"

    suspend fun getAvailableCategories(): List<Category> = withContext(Dispatchers.IO) {
        val (_, _, result) = Fuel.get(urlGetAvailableCategories).responseString()
        val jsonResponse = JsonParser().parse(result.get()).asJsonArray
        return@withContext jsonResponse.map { Category(it.asJsonObject) }
    }

    suspend fun getChildLimits(kidId: Int): List<Limit> = withContext(Dispatchers.IO) {
        val (_, _, result) = Fuel.get(urlGetKidLimits.format(kidId)).responseString()
        val jsonResponse = JsonParser().parse(result.get()).asJsonArray
        return@withContext jsonResponse.map { Limit(it.asJsonObject) }
    }

    suspend fun addChildLimit(kidId: Int, categoryId: Int, limit: Int): Boolean = withContext(Dispatchers.IO) {
        val (_, response, _) = Fuel.post(urlAddKidLimits.format(kidId))
            .body(Gson().toJson(mapOf("categoryId" to categoryId, "limit" to limit)))
            .header(Pair("Content-Type", "application/json"))
            .responseString()
        return@withContext response.statusCode == 200
    }

    suspend fun setChildLimit(kidId: Int, limitId: Int, limit: Int): Boolean = withContext(Dispatchers.IO) {
        val (_, response, _) = Fuel.put(urlSetKidLimits.format(kidId))
            .body(Gson().toJson(mapOf("id" to limitId, "limit" to limit)))
            .header(Pair("Content-Type", "application/json"))
            .responseString()
        return@withContext response.statusCode == 200
    }

    suspend fun deleteChildLimit(kidId: Int, limitId: Int): Boolean = withContext(Dispatchers.IO) {
        val (_, response, _) = Fuel.delete(urlDeleteKidLimits.format(kidId, limitId)).responseString()
        return@withContext response.statusCode == 200
    }

    suspend fun getKids(): List<Kid> = withContext(Dispatchers.IO) {
        val (_, _, result) = Fuel.get(urlGetUserKids, listOf("parentId" to 1)).responseString()
        val jsonResponse = JsonParser().parse(result.get()).asJsonArray
        return@withContext jsonResponse.map { Kid(it.asJsonObject) }
    }

    suspend fun addKid(phone: String, name: String): Boolean = withContext(Dispatchers.IO) {
        val (_, response, _) = Fuel.post(urlRegisterKid)
            .body(Gson().toJson(mapOf("parentId" to 1, "name" to name, "phone" to phone)))
            .header(Pair("Content-Type", "application/json"))
            .responseString()
        return@withContext response.statusCode == 200
    }

    suspend fun getNotification(): JsonObject? = withContext(Dispatchers.IO) {
        val (_, _, result) = Fuel.get(getNotification).responseString()
        val jsonResponse = JsonParser().parse(result.get())
        if (jsonResponse.isJsonObject) {
            return@withContext jsonResponse.asJsonObject
        } else {
            return@withContext null
        }
    }
}