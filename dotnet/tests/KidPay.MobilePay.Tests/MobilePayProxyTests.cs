using System;
using System.Threading.Tasks;
using KidPay.MobilePay;
using Xunit;

namespace KidPay.MobilePay.Tests
{
    public class MobilePayProxyTests
    {
        private readonly MobilePaySettings _settings = new MobilePaySettings
        {
            AccessToken = @"eyJhbGciOiJSUzI1NiIsImtpZCI6IjQyRTlDQTMxMkVBMzg1OUY4REMzNjVFNDg3OEI0MjY1Qjg5MTREOTQiLCJ0eXAiOiJKV1QiLCJ4NXQiOiJRdW5LTVM2amhaLU53MlhraDR0Q1piaVJUWlEifQ.eyJuYmYiOjE1NDMwNzUxNjYsImV4cCI
6MTU0MzA3NTQ2NiwiaXNzIjoiaHR0cHM6Ly9hcGkubW9iaWxlcGF5LmRrL21lcmNoYW50IiwiYXVkIjoiaHR0cHM6Ly9hcGkubW9iaWxlcGF5LmRrL21lcmNoYW50L3Jlc291cmNlcyIsImNsaWVudF9pZCI6IkhhY2thdGhvbjA1Iiwic3ViI
joiOTFjY2U0OGYtODQyNC00MzkwLWI1MmEtMzk3MGVmNWY0ODk2IiwiYXV0aF90aW1lIjoxNTQzMDYyMzQxLCJpZHAiOiJsb2NhbCIsInNjb3BlIjpbIm9wZW5pZCIsImludm9pY2UiLCJzdWJzY3JpcHRpb25zIl0sImFtciI6WyJVc2VyRW5
0ZXJlZENvZGUiLCJTZXJ2ZXJQcm92aWRlZEtleSJdfQ.gzx5fYkaWlPp2S5q6R1lO08J_qDNAkFzcoU8PogbzXG_RBOAsWPJ7WgmLDZZXGgKbGrRBbshjDZYxAOFcqeFv-aDR_ow2sQavu7zadOHLs4qvEYSjPs3OwfHMl2jVQbw20LnAtLbkr
A7e5rKo1HLEk45CkndmrTbFBItYXMGev1L5mnMWBVozmxeWLIMLGYQ0s3b3uHNEQefYKWzp2mK8pB80D6GcpAnmaGaQLVIMILAOiAyBgJ34J_R2qija0iy851n52LmB0ntQMewJBPTIaV053LuSUVr8N9LJQS5Mp7QdBr0QUwfwOXgdAp1VxgS
rDdSgIckvuxKy_bJRRffvA",
            MerchantId = "2637ba29-7741-4e47-a83d-d79b3158be1e",
            ClientId = "fa462891-87a3-4602-9de2-a8b63f3b0b36",
            ClientSecret = "lI0eE6jI7rM6dO7tG6eR4oT3jB5lL8dS3gL7gD5wM8hG0sT0nB",
            MobileAppApiBaseUrl = "https://api.sandbox.mobilepay.dk/invoice-restapi",
            InvoiceIssuer = "bb68362f-a0ae-47c0-aec6-6f073a5f463c",
        }; 
        
        [Fact]
        public async Task Test1()
        {
            var proxy = new MobilePayProxy(_settings);

            await proxy.ProcessInvoiceAsync();
        }
    }
}