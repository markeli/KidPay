using Autofac;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;

namespace KidPay.Core
{
    public class LoggingModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var loggerFactory = new LoggerFactory();
            loggerFactory.AddNLog(new NLogProviderOptions(){CaptureMessageTemplates = false, CaptureMessageProperties = false});
            loggerFactory.ConfigureNLog("NLog.config");

            builder.RegisterInstance(loggerFactory).As<ILoggerFactory>();
            builder.RegisterInstance(loggerFactory.CreateLogger("main")).As<ILogger>();
        }
    }
}