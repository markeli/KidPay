using System.Collections.Generic;
using Newtonsoft.Json;

namespace KidPay.Core.Data.Entities
{
    public class CategoryEntity
    {
        public long Id { get; set; }
        
        public string Name { get; set; }
        
        [JsonIgnore]
        public virtual ICollection<ChildLimitEntity> Limits { get; set; }
    }
}