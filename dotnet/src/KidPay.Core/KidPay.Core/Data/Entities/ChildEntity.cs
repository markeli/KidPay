using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace KidPay.Core.Data.Entities
{
    public class ChildEntity
    {
        public long Id { get; set; }
        
        public long ParentId { get; set; }
        
        public string Name { get; set; }
                
        public string Phone { get; set; }
        
        [JsonIgnore]
        public virtual ParentEntity Parent { get; set; }
        
        [JsonIgnore]
        public virtual ICollection<ChildLimitEntity> Limits { get; set; }
    }
}