using System.Collections.Generic;
using Newtonsoft.Json;

namespace KidPay.Core.Data.Entities
{
    public class ParentEntity
    {
        public long Id { get; set; }
        
        public string Name { get; set; }
        
        public string Email { get; set; }

        public string Phone { get; set; }
        
        public virtual ICollection<ChildEntity> Children { get; set; }
    }
}