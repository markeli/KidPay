using Newtonsoft.Json;

namespace KidPay.Core.Data.Entities
{
    public class ChildLimitEntity
    {
        public long Id { get; set; }
                
        public long ChildId { get; set; }
        
        public long CategoryId { get; set; }
        
        public decimal Limit { get; set; }
        
        public decimal CurrentAmount { get; set; }
        
        [JsonIgnore]
        public virtual ChildEntity Child { get; set; }
        
        [JsonIgnore]
        public virtual CategoryEntity Category { get; set; }
    }
}