using Autofac;
using Microsoft.EntityFrameworkCore;

namespace KidPay.Core.Data
{
    internal class DataModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var dbOptionsBuilder = new DbContextOptionsBuilder<KidPayContext>();;

            dbOptionsBuilder.UseInMemoryDatabase("test");
           
            var contextFactory = new KidPayContextFactory(dbOptionsBuilder.Options);
            builder.RegisterInstance(contextFactory).As<IKidPayContextFactory>();
        }
    }
}
