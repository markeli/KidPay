using KidPay.Core.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace KidPay.Core.Data
{
    public class KidPayContext : DbContext
    {
        public KidPayContext(DbContextOptions<KidPayContext> options) : base(options) {}

        public DbSet<ParentEntity> Parents { get; set; }

        public DbSet<ChildEntity> Children { get; set; }
        
        public DbSet<ChildLimitEntity> ChildrenLimits { get; set; }
        
        public DbSet<CategoryEntity> Categories { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ParentEntity>()
                .ToTable("parents")
                .HasKey(p => p.Id);
            modelBuilder.Entity<ParentEntity>()
                .HasMany(p => p.Children)
                .WithOne(c => c.Parent)
                .HasForeignKey(c => c.ParentId);
            
            modelBuilder.Entity<ChildEntity>()
                .ToTable("children")
                .HasKey(c => c.Id);
            modelBuilder.Entity<ChildEntity>()
                .HasMany(c => c.Limits)
                .WithOne(l => l.Child)
                .HasForeignKey(c => c.ChildId);
            
            modelBuilder.Entity<CategoryEntity>()
                .ToTable("categories")
                .HasKey(c => c.Id);
            modelBuilder.Entity<CategoryEntity>()
                .HasMany(c => c.Limits)
                .WithOne(l => l.Category)
                .HasForeignKey(c => c.CategoryId);
            
            modelBuilder.Entity<ChildLimitEntity>()
                .ToTable("child_limits")
                .HasKey(c => c.Id);
        }
    }
}