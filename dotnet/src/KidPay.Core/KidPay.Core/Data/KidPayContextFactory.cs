using System;
using Microsoft.EntityFrameworkCore;

namespace KidPay.Core.Data
{
    public interface IKidPayContextFactory
    {
        KidPayContext CreateContext();
    }

    internal class KidPayContextFactory : IKidPayContextFactory
    {
        private readonly DbContextOptions<KidPayContext> _options;

        public KidPayContextFactory(DbContextOptions<KidPayContext> options)
        {
            _options = options ?? throw new ArgumentNullException(nameof(options));
        }

        public KidPayContext CreateContext()
        {
            return new KidPayContext(_options);
        }
    }
}
