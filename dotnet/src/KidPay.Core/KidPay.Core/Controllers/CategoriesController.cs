﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KidPay.Core.Data;
using KidPay.Core.Data.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace KidPay.Core.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly IKidPayContextFactory _contextFactory;

        public CategoriesController(IKidPayContextFactory contextFactory)
        {
            _contextFactory = contextFactory ?? throw new ArgumentNullException(nameof(contextFactory));
        }

        /// <summary>
        /// Get all existing categories
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(CategoryEntity[]), 200)]
        public async Task<IActionResult> GetAsync()
        {
            using (var context = _contextFactory.CreateContext())
            {
                var categories = await context.Categories.ToArrayAsync();
                return Ok(categories);
            }
        }
    }
}
