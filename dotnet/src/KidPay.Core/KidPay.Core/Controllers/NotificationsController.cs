using System.Collections.Generic;
using System.Threading.Tasks;
using IO.Swagger.Model;
using Microsoft.AspNetCore.Mvc;

namespace KidPay.Core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationsController : ControllerBase
    {
        /// <summary>
        /// Send new payment invoice by child
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(NotificationModel), 200)]
        [ProducesResponseType(204)]
        public IActionResult GetNotification()
        {
            if (NotificationQueue.Notifications.TryDequeue(out var noti))
            {
                return Ok(noti);
            }
            else
            {
                return NoContent();
            }
        }
    }

    public class NotificationModel
    {
        public string Title { get; set; }
        public string Body { get; set; }
        
        public long ChildId { get; set; }
        
        public string Name { get; set; }
        
        public string Phone { get; set; }
    }

    internal static class NotificationQueue
    {
        private static Queue<NotificationModel> _notifications = new Queue<NotificationModel>();
        
        public static Queue<NotificationModel> Notifications
        {
            get => _notifications;
        }
    }
}