using System;
using System.Linq;
using System.Threading.Tasks;
using KidPay.Core.Data;
using KidPay.Core.Data.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace KidPay.Core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChildrenController : ControllerBase
    {
        private readonly IKidPayContextFactory _contextFactory;

        public ChildrenController(IKidPayContextFactory contextFactory)
        {
            _contextFactory = contextFactory ?? throw new ArgumentNullException(nameof(contextFactory));
        }

        /// <summary>
        /// Add new child
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AddChildAsync(ChildEntity child)
        {
            using (var context = _contextFactory.CreateContext())
            {
                await context.Children.AddAsync(child);
                await context.SaveChangesAsync();
                
                return Ok(child);
            }
        }
        
        /// <summary>
        /// Get children list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(ChildEntity[]), 200)]
        public async Task<IActionResult> GetChildrenAsync(long parentId)
        {
            using (var context = _contextFactory.CreateContext())
            {
                var children = await context
                    .Children
                    .Where(c => c.ParentId == parentId)
                    .ToArrayAsync();

                return Ok(children);
            }
        }
        
        /// <summary>
        /// Get child's limits
        /// </summary>
        /// <returns></returns>
        [HttpGet("{childId}/limits")]
        [ProducesResponseType(typeof(ChildLimitEntity[]), 200)]
        public async Task<IActionResult> GetChildLimitsAsync(long childId)
        {
            using (var context = _contextFactory.CreateContext())
            {
                var childLimits = await context
                    .ChildrenLimits
                    .Where(l => l.ChildId == childId)
                    .ToArrayAsync();

                return Ok(childLimits);
            }
        }
        
        /// <summary>
        /// Add child's limit
        /// </summary>
        /// <returns></returns>
        [HttpPost("{childId}/limits")]
        [ProducesResponseType(typeof(ChildLimitEntity), 200)]
        public async Task<IActionResult> AddChildLimitsAsync(long childId, ChildLimitEntity childLimit)
        {
            using (var context = _contextFactory.CreateContext())
            {
                if (!context.Children.Any(c => c.Id == childId))
                    return BadRequest("No such child");

                childLimit.ChildId = childId;
                await context.ChildrenLimits.AddAsync(childLimit);
                await context.SaveChangesAsync();

                return Ok(childLimit);
            }
        }
        
        /// <summary>
        /// Edit child's limit
        /// </summary>
        /// <returns></returns>
        [HttpPut("{childId}/limits")]
        [ProducesResponseType(typeof(ChildLimitEntity), 200)]
        public async Task<IActionResult> EditChildLimitAsync(long childId, ChildLimitEntity childLimit)
        {
            using (var context = _contextFactory.CreateContext())
            {
                if (!context.Children.Any(c => c.Id == childId))
                    return BadRequest("No such child");

                var existingLimit = await context.ChildrenLimits.FirstOrDefaultAsync(l => l.Id == childLimit.Id);
                existingLimit.Limit = childLimit.Limit;
                
                childLimit.ChildId = childId;
                context.ChildrenLimits.Update(existingLimit);
                await context.SaveChangesAsync();

                return Ok(existingLimit);
            }
        }

        /// <summary>
        /// Remove child's limit
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{childId}/limits/{limitId}")]
        public async Task<IActionResult> DeleteChildLimitAsync(long childId, long limitId)
        {
            using (var context = _contextFactory.CreateContext())
            {
                if (!context.Children.Any(c => c.Id == childId))
                    return NotFound("No such child");

                var limitEntity = await context.ChildrenLimits.FirstOrDefaultAsync(l => l.Id == limitId && l.ChildId == childId);
                if (limitEntity == null)
                    return NotFound("No such limit");
                
                context.Remove(limitEntity);
                await context.SaveChangesAsync();

                return Ok(limitEntity);
            }
        }
    }
}