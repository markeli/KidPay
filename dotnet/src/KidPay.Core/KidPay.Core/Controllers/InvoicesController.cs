using System;
using System.Threading.Tasks;
using IO.Swagger.Model;
using KidPay.Core.Data;
using KidPay.Core.Services;
using KidPay.MobilePay;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace KidPay.Core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvoicesController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly IMarvinInvoiceProcService _marvinInvoiceProcService;
        private readonly IMobilePayProxy _mobilePayInvoiceProcService;
        private readonly IParentNotificationService _parentNotificationService;
        private readonly IInvoiceStatusWatchDog _watchDog;
        private readonly IKidPayContextFactory _contextFactory;

        public InvoicesController(
            ILogger logger, 
            IMarvinInvoiceProcService marvinInvoiceProcService, 
            IMobilePayProxy mobilePayInvoiceProcService, 
            IParentNotificationService parentNotificationService, 
            IInvoiceStatusWatchDog watchDog, 
            IKidPayContextFactory contextFactory)
        {
            _logger = logger;
            _marvinInvoiceProcService = marvinInvoiceProcService;
            _mobilePayInvoiceProcService = mobilePayInvoiceProcService;
            _parentNotificationService = parentNotificationService;
            _watchDog = watchDog;
            _contextFactory = contextFactory;
        }

        /// <summary>
        /// Send new payment invoice by child
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(InvoiceResultModel), 200)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> ProcessInvoice(InvoiceModel invoice)
        {
            if (invoice == null) return BadRequest();

            using (var context = _contextFactory.CreateContext())
            {
                var category = await context.Categories.FirstOrDefaultAsync(c => c.Id == invoice.CategoryId);
                if (category == null)
                    return NotFound($"Category not found for merchant with Id {invoice.MerchantId}");
                
                var child = await context.Children
                    .Include(x => x.Parent)
                    .FirstOrDefaultAsync(x => String.Equals(x.Phone, invoice.Phone));
                if (child == null)
                    return BadRequest($"User with phone {invoice.Phone} not found");
                
                var isLimitNotReached =
                    await _marvinInvoiceProcService
                        .CheckLimitAsync(child.Id, category.Id, invoice.Amount);
                
                if (!isLimitNotReached)
                {
                    await _parentNotificationService.NotifyAboutLimitsReachingAsync(
                        child.Id,
                        category.Id);
                    return new StatusCodeResult(402);
                }

                try
                {
                    var processingResult =
                        await _mobilePayInvoiceProcService.ProcessInvoiceAsync();

                    var parentId = child.Parent.Id;
                    _watchDog.StartWatch(
                        processingResult, 
                        parentId,
                        child.Id,
                        invoice.Amount,
                        category.Id);
                
                    return Ok(processingResult);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Error while sending invoice to MobilePay");
                    return new StatusCodeResult(500);
                }   
            }
        }
    }

    public class InvoiceModel
    {
        public long MerchantId { get; set; }
        
        public long CategoryId { get; set; }

        public string Phone { get; set; }
        
        public decimal Amount { get; set; }
    }
}