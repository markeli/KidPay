﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace KidPay.Core
{
    public class Program
    {
        public static void Main(string[] args)
        {            
            BuildWebHost(args).Run();
        }

        private static IWebHost BuildWebHost(string[] args)
        {
            var configProvider = new ConfigProvider();
            var config = configProvider.GetConfiguration();
            
            var uriBuilder = new UriBuilder();
            uriBuilder.Host = config.GetValue<string>("api_host");
            uriBuilder.Port = config.GetValue<int>("api_port");
            
            return WebHost.CreateDefaultBuilder(args)
                .ConfigureServices(s => s.AddAutofac())
                .UseStartup<Startup>()
                .UseUrls(uriBuilder.Uri.ToString())
                .Build();
        }
    }

    internal class ConfigProvider
    {
        private readonly IConfigurationBuilder _builder;

        public ConfigProvider()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Environment.CurrentDirectory)
                .AddJsonFile("appsettings.json");
            
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var envConfigName = $"appsettings.{environment}.json";
            if(!string.IsNullOrWhiteSpace(environment))
                builder.AddJsonFile(envConfigName);

            builder.AddJsonFile("mobilepay.json");

            _builder = builder;
        }

        public IConfiguration GetConfiguration()
        {
            return _builder.Build();
        }
    }
}
