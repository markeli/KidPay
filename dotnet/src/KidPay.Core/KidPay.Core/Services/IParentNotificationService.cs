using System.Threading.Tasks;

namespace KidPay.Core.Services
{
    /// <summary>
    /// Service to notify parents about children financial activity
    /// </summary>
    public interface IParentNotificationService
    {
        /// <summary>
        /// Send notification to parent when limit on some category have been reached
        /// </summary>
        /// <param name="childId"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        Task NotifyAboutLimitsReachingAsync(long childId, long categoryId);

        /// <summary>
        /// Notify parent about processed child's invoice
        /// </summary>
        /// <param name="childId"></param>
        /// <param name="amount"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        Task NotifyAboutInvoiceAsync(long childId, decimal amount, long categoryId);
    }
}