using Autofac;
using KidPay.MobilePay;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace KidPay.Core.Services
{
    /// <summary>
    /// IoC module with all domain classes
    /// </summary>
    public class ServiceModule : Module
    {
        /// <inheritdoc />
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<GenuisProxy>().As<IMobilePayProxy>().SingleInstance();
            builder.RegisterType<ParentNotificationService>().As<IParentNotificationService>().SingleInstance();
            builder.RegisterType<InvoiceStatusWatchdog>()
                .As<IInvoiceStatusWatchDog>()
                .As<IHostedService>()
                .SingleInstance();
            builder.RegisterType<MarvinInvoiceProcService>().As<IMarvinInvoiceProcService>().SingleInstance();

            var config = new ConfigProvider().GetConfiguration();
            
            var mobileAppSettings = new MobilePaySettings();
            config.Bind(mobileAppSettings);

            builder.RegisterInstance(mobileAppSettings);
        }
    }
}