using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FcmSharp;
using FcmSharp.Requests;
using FcmSharp.Settings;
using IO.Swagger.Model;
using KidPay.Core.Controllers;
using KidPay.Core.Data;
using KidPay.MobilePay;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace KidPay.Core.Services
{
    /// <inheritdoc />
    internal class GenuisProxy: IMobilePayProxy
    {
        /// <inheritdoc />
        public Task<Guid> ProcessInvoiceAsync()
        {
            return Task.FromResult(Guid.NewGuid());
        }

        /// <inheritdoc />
        public Task<InvoiceStatusResultModel> GetStatusAsync(Guid invoiceId)
        {
            return Task.FromResult(new InvoiceStatusResultModel
            {
                InvoiceId = invoiceId,
                Status = "accepted"
            });
        }

        /// <inheritdoc />
        public Task AcceptInvoiceByCustomerAsync()
        {
            return Task.CompletedTask;
        }

        /// <inheritdoc />
        public Task RejectInvoiceByCustomerAsync()
        {
            return Task.CompletedTask;
        }
    }
    
    /// <inheritdoc />
    internal class ParentNotificationService : IParentNotificationService
    {
        private readonly IKidPayContextFactory _contextFactory;
        private readonly ILogger _logger;

        /// <inheritdoc />
        public ParentNotificationService(
            IKidPayContextFactory contextFactory, 
            ILogger logger)
        {
            _contextFactory = contextFactory ?? throw new ArgumentNullException(nameof(contextFactory));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        /// <inheritdoc />
        public async Task NotifyAboutLimitsReachingAsync(long childId, long categoryId)
        {
            using (var context = _contextFactory.CreateContext())
            {
                var limit = await context
                    .ChildrenLimits
                    .Include(l => l.Child)
                    .Include(l => l.Category)
                    .FirstOrDefaultAsync(l => l.CategoryId == categoryId && l.ChildId == childId);

                if (limit == null)
                {
                    _logger.LogWarning($"Limit for child {childId} and category {categoryId} not found");
                    return;
                }
                
                var notification = new NotificationModel
                {
                    Title = "Your child try to overcome limit!",
                    Body = $"{limit.Child.Name} trying to overcome limit in {limit.Category.Name} category!",
                    ChildId = childId,
                    Name = limit.Child.Name,
                    Phone = limit.Child.Phone
                };

                NotificationQueue.Notifications.Enqueue(notification);
                _logger.LogInformation($"Notification for child {childId} sent: {notification.Body}");
            }
        }

        /// <inheritdoc />
        public async Task NotifyAboutInvoiceAsync(long childId, decimal amount, long categoryId)
        {
            using (var context = _contextFactory.CreateContext())
            {
                var child = await context
                    .Children
                    .FirstOrDefaultAsync(c => c.Id == childId);
                
                var category = await context
                    .Categories
                    .FirstOrDefaultAsync(c => c.Id == categoryId);

                if (child == null)
                {
                    _logger.LogWarning($"Child {childId} not founds");
                    return;
                }
                
                var notification = new NotificationModel
                {
                    Title = "Your child is purchasing",
                    Body = $"{child.Name} just spent {amount} euros in {category?.Name} category!",
                    ChildId = childId,
                    Name = child.Name,
                    Phone = child.Phone
                };

                NotificationQueue.Notifications.Enqueue(notification);
                _logger.LogInformation($"Notification for child {child} sent: {notification.Body}");
            }
        }
    }
}