using System;
using Microsoft.Extensions.Hosting;

namespace KidPay.Core.Services
{
    /// <summary>
    /// Monitoring invoice and on completion of process send notifications to parents
    /// </summary>
    /// <remarks>
    /// On invoice processing completion remove invoice from watch list
    /// </remarks>
    public interface IInvoiceStatusWatchDog : IHostedService
    {
        /// <summary>
        /// Start monitoring invoice status
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <param name="parentId"></param>
        /// <param name="childId"></param>
        /// <param name="amount"></param>
        /// <param name="categoryId"></param>
        void StartWatch(
            Guid invoiceId, 
            long parentId, 
            long childId,
            decimal amount,
            long categoryId);
    }
}