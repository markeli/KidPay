using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using KidPay.MobilePay;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace KidPay.Core.Services
{
    /// <inheritdoc cref="IInvoiceStatusWatchDog" />
    public class InvoiceStatusWatchdog : BackgroundService, IInvoiceStatusWatchDog
    {
        private readonly ConcurrentQueue<InvoiceWatchDogParams> _invoiceToWatch;
        private readonly IMobilePayProxy _mobilePayProxy;
        private readonly ILogger _logger;
        private readonly IParentNotificationService _parentNotificationService;
        private readonly IMarvinInvoiceProcService _marvinInvoiceProcService;

        /// <inheritdoc />
        public InvoiceStatusWatchdog(
            ILogger logger, 
            IParentNotificationService parentNotificationService, 
            IMobilePayProxy mobilePayProxy, 
            IMarvinInvoiceProcService marvinInvoiceProcService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _parentNotificationService = parentNotificationService ?? throw new ArgumentNullException(nameof(parentNotificationService));
            _mobilePayProxy = mobilePayProxy ?? throw new ArgumentNullException(nameof(mobilePayProxy));
            _marvinInvoiceProcService = marvinInvoiceProcService ?? throw new ArgumentNullException(nameof(marvinInvoiceProcService));
            _invoiceToWatch = new ConcurrentQueue<InvoiceWatchDogParams>();
        }

        /// <inheritdoc />
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Yield();

            while (!stoppingToken.IsCancellationRequested)
            {
                InvoiceWatchDogParams invoice = null;
                try
                {
                    while (_invoiceToWatch.TryDequeue(out invoice))
                    {
                        var status = await _mobilePayProxy.GetStatusAsync(invoice.InvoiceId);
                        if (String.Equals(status.Status, "accepted", StringComparison.InvariantCultureIgnoreCase))
                        {
                            var isUpdated = await _marvinInvoiceProcService.UpdateLimitAsync(
                                invoice.ChildId, 
                                invoice.CategoryId, 
                                invoice.Amount);
                            if (!isUpdated)
                            {
                                _logger.LogWarning($"Can not update limit for child {invoice.ChildId} for category {invoice.CategoryId}");
                            }
                            await _parentNotificationService.NotifyAboutInvoiceAsync(
                                invoice.ChildId,
                                invoice.Amount,
                                invoice.CategoryId);
                        }

                        // if not processed - stay watching
                        if (String.Equals(status.Status, "created", StringComparison.InvariantCultureIgnoreCase))
                        {
                            _invoiceToWatch.Enqueue(invoice);
                        }
                    }

                    await Task.Delay(TimeSpan.FromSeconds(10), stoppingToken);
                }
                catch (Exception e)
                {
                    
                    _logger.LogError(e, "Error while checking invoice status");

                    if (invoice != null)
                    {
                        _invoiceToWatch.Enqueue(invoice);
                    }
                }
            }
        }

        /// <inheritdoc />
        public void StartWatch(
            Guid invoiceId, 
            long parentId,
            long childId,
            decimal amount,
            long categoryId)
        {
            _invoiceToWatch.Enqueue(new InvoiceWatchDogParams
            {
                InvoiceId = invoiceId,
                ParentId = parentId,
                ChildId = childId,
                CategoryId = categoryId,
                Amount = amount
            });
        }

        /// <summary>
        /// Required for monitoring information about invoice
        /// </summary>
        private class InvoiceWatchDogParams
        {
            public Guid InvoiceId { get; set; }
            
            public long ParentId { get; set; }
            
            public long ChildId { get; set; }
            
            public decimal Amount { get; set; }
            
            public long CategoryId { get; set; }
        }
    }
}