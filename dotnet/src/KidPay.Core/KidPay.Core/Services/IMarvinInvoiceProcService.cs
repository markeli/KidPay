using System.Threading.Tasks;

namespace KidPay.Core.Services
{
    /// <summary>
    /// Marvin's invoice processing service
    /// </summary>
    public interface IMarvinInvoiceProcService
    {
        /// <summary>
        /// Check limits before invoice send
        /// </summary>
        /// <returns></returns>
        Task<bool> CheckLimitAsync(long childId, long categoryId, decimal amount);

        /// <summary>
        /// Update child's limit after pay
        /// </summary>
        /// <returns></returns>
        Task<bool> UpdateLimitAsync(long childId, long categoryId, decimal amount);
    }
}