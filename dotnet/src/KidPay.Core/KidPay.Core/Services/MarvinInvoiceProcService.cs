using System;
using System.Linq;
using System.Threading.Tasks;
using KidPay.Core.Data;
using Microsoft.EntityFrameworkCore;

namespace KidPay.Core.Services
{
    /// <inheritdoc />
    internal class MarvinInvoiceProcService : IMarvinInvoiceProcService
    {
        private readonly IKidPayContextFactory _contextFactory;

        public MarvinInvoiceProcService(IKidPayContextFactory contextFactory)
        {
            _contextFactory = contextFactory ?? throw new ArgumentNullException(nameof(contextFactory));
        }
        
        /// <summary>
        /// Check limits before invoice send
        /// </summary>
        /// <returns></returns>
        public async Task<bool> CheckLimitAsync(long childId, long categoryId, decimal amount)
        {
            using (var context = _contextFactory.CreateContext())
            {
                var limit = await context
                    .ChildrenLimits
                    .Where(l => l.Child.Id == childId && l.CategoryId == categoryId)
                    .FirstOrDefaultAsync();

                if (limit == null) return false;

                var availableMoney = limit.Limit - limit.CurrentAmount;
                return amount <= availableMoney;
            }
        }

        /// <summary>
        /// Update child's limit after pay
        /// </summary>
        /// <returns></returns>
        public async Task<bool> UpdateLimitAsync(long childId, long categoryId, decimal amount)
        {
            using (var context = _contextFactory.CreateContext())
            {
                var limit = await context
                    .ChildrenLimits
                    .Where(l => l.Child.Id == childId && l.CategoryId == categoryId)
                    .FirstOrDefaultAsync();

                if (limit == null) return false;

                limit.CurrentAmount += amount;
                context.ChildrenLimits.Update(limit);

                await context.SaveChangesAsync();
                return true;
            }
        }
    }
}