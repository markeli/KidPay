﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using KidPay.Core.Data;
using KidPay.Core.Data.Entities;
using KidPay.Core.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace KidPay.Core
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            CurrentEnvironment = env;
        }
        
        private IHostingEnvironment CurrentEnvironment { get; }

        private string ApiTitle = nameof(KidPay) + " Api";
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddLogging();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(nameof(KidPay), new Info(){ Title = ApiTitle, Version = "0.0.0.1"});

                //XML docs
                var basePath = AppContext.BaseDirectory;
                var xmlDocs = Directory.GetFiles(basePath)
                    .Where(fn => fn.Contains(nameof(KidPay.Core)) && fn.ToUpper().EndsWith(".XML"));

                foreach (var xmlDoc in xmlDocs)
                {
                    c.IncludeXmlComments(xmlDoc);
                }
            });
            
            if (CurrentEnvironment.IsDevelopment())
            {
                services.AddLogging(b => b.SetMinimumLevel(LogLevel.Trace));
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseDeveloperExceptionPage();

            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint($"/swagger/{nameof(KidPay)}/swagger.json", ApiTitle); });

            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var contextFactory = serviceScope.ServiceProvider.GetRequiredService<IKidPayContextFactory>();
                GenerateFakeData(contextFactory);
            }
            
            app.UseMvc();
        }
        
        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new DataModule());
            builder.RegisterModule(new ServiceModule());
            builder.RegisterModule(new LoggingModule());
        }

        private void GenerateFakeData(IKidPayContextFactory contextFactory)
        {
            using (var context = contextFactory.CreateContext())
            {
                context.Database.EnsureCreated();

                context.Parents.Add(new ParentEntity()
                {
                    Id = 1,
                    Email = "john_dow@gmail.com",
                    Name = "John Dow",
                    Phone = "+7-555-432-11-12"
                });
                
                context.Categories.AddRange(new []
                {
                    new CategoryEntity()
                    {
                        Name = "Candy, Nut, and Confectionery Stores"
                    }, 
                    new CategoryEntity()
                    {
                        Name = "Fast Food Restaurants"
                    }, 
                    new CategoryEntity()
                    {
                        Name = "Bus Lines"
                    }
                });

                context.SaveChanges();
            }
        }
    }
}
