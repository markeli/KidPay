using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// Errors related to basic request validation like authorization headers, ClientId, ClientSecret validations. Not a business logic.
  /// </summary>
  [DataContract]
  public class BasicError {
    /// <summary>
    /// HTTP status code.
    /// </summary>
    /// <value>HTTP status code.</value>
    [DataMember(Name="httpCode", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "httpCode")]
    public string HttpCode { get; set; }

    /// <summary>
    /// HTTP status code description.
    /// </summary>
    /// <value>HTTP status code description.</value>
    [DataMember(Name="httpMessage", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "httpMessage")]
    public string HttpMessage { get; set; }

    /// <summary>
    /// More detailed explanation.
    /// </summary>
    /// <value>More detailed explanation.</value>
    [DataMember(Name="moreInformation", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "moreInformation")]
    public string MoreInformation { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BasicError {\n");
      sb.Append("  HttpCode: ").Append(HttpCode).Append("\n");
      sb.Append("  HttpMessage: ").Append(HttpMessage).Append("\n");
      sb.Append("  MoreInformation: ").Append(MoreInformation).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
