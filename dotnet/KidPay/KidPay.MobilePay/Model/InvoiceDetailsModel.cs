using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class InvoiceDetailsModel {
    /// <summary>
    /// Gets or Sets InvoiceId
    /// </summary>
    [DataMember(Name="InvoiceId", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "InvoiceId")]
    public Guid? InvoiceId { get; set; }

    /// <summary>
    /// Gets or Sets InvoiceNumber
    /// </summary>
    [DataMember(Name="InvoiceNumber", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "InvoiceNumber")]
    public string InvoiceNumber { get; set; }

    /// <summary>
    /// Gets or Sets IssueDate
    /// </summary>
    [DataMember(Name="IssueDate", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "IssueDate")]
    public DateTime? IssueDate { get; set; }

    /// <summary>
    /// Gets or Sets DueDate
    /// </summary>
    [DataMember(Name="DueDate", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "DueDate")]
    public DateTime? DueDate { get; set; }

    /// <summary>
    /// Gets or Sets Comment
    /// </summary>
    [DataMember(Name="Comment", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "Comment")]
    public string Comment { get; set; }

    /// <summary>
    /// Gets or Sets InvoiceArticles
    /// </summary>
    [DataMember(Name="InvoiceArticles", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "InvoiceArticles")]
    public List<InvoiceArticleDetailModel> InvoiceArticles { get; set; }

    /// <summary>
    /// Gets or Sets CurrencyCode
    /// </summary>
    [DataMember(Name="CurrencyCode", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "CurrencyCode")]
    public string CurrencyCode { get; set; }

    /// <summary>
    /// Gets or Sets TotalAmount
    /// </summary>
    [DataMember(Name="TotalAmount", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "TotalAmount")]
    public double? TotalAmount { get; set; }

    /// <summary>
    /// Gets or Sets InvoiceVatTotals
    /// </summary>
    [DataMember(Name="InvoiceVatTotals", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "InvoiceVatTotals")]
    public List<InvoiceVatRateTotalModel> InvoiceVatTotals { get; set; }

    /// <summary>
    /// Gets or Sets TotalVatAmount
    /// </summary>
    [DataMember(Name="TotalVatAmount", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "TotalVatAmount")]
    public double? TotalVatAmount { get; set; }

    /// <summary>
    /// Gets or Sets TotalAmountExcludingVat
    /// </summary>
    [DataMember(Name="TotalAmountExcludingVat", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "TotalAmountExcludingVat")]
    public double? TotalAmountExcludingVat { get; set; }

    /// <summary>
    /// Gets or Sets MerchantId
    /// </summary>
    [DataMember(Name="MerchantId", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "MerchantId")]
    public Guid? MerchantId { get; set; }

    /// <summary>
    /// Gets or Sets InvoiceIssuerId
    /// </summary>
    [DataMember(Name="InvoiceIssuerId", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "InvoiceIssuerId")]
    public Guid? InvoiceIssuerId { get; set; }

    /// <summary>
    /// Gets or Sets InvoiceIssuerName
    /// </summary>
    [DataMember(Name="InvoiceIssuerName", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "InvoiceIssuerName")]
    public string InvoiceIssuerName { get; set; }

    /// <summary>
    /// Gets or Sets InvoiceIssuerAddress
    /// </summary>
    [DataMember(Name="InvoiceIssuerAddress", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "InvoiceIssuerAddress")]
    public string InvoiceIssuerAddress { get; set; }

    /// <summary>
    /// Gets or Sets InvoiceIssuerZipcode
    /// </summary>
    [DataMember(Name="InvoiceIssuerZipcode", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "InvoiceIssuerZipcode")]
    public string InvoiceIssuerZipcode { get; set; }

    /// <summary>
    /// Gets or Sets InvoiceIssuerCity
    /// </summary>
    [DataMember(Name="InvoiceIssuerCity", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "InvoiceIssuerCity")]
    public string InvoiceIssuerCity { get; set; }

    /// <summary>
    /// Gets or Sets MerchantIsoCountryCode
    /// </summary>
    [DataMember(Name="MerchantIsoCountryCode", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "MerchantIsoCountryCode")]
    public string MerchantIsoCountryCode { get; set; }

    /// <summary>
    /// Gets or Sets LogoUrl
    /// </summary>
    [DataMember(Name="LogoUrl", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "LogoUrl")]
    public string LogoUrl { get; set; }

    /// <summary>
    /// Gets or Sets Status
    /// </summary>
    [DataMember(Name="Status", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "Status")]
    public string Status { get; set; }

    /// <summary>
    /// Gets or Sets InvoiceUrl
    /// </summary>
    [DataMember(Name="InvoiceUrl", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "InvoiceUrl")]
    public string InvoiceUrl { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class InvoiceDetailsModel {\n");
      sb.Append("  InvoiceId: ").Append(InvoiceId).Append("\n");
      sb.Append("  InvoiceNumber: ").Append(InvoiceNumber).Append("\n");
      sb.Append("  IssueDate: ").Append(IssueDate).Append("\n");
      sb.Append("  DueDate: ").Append(DueDate).Append("\n");
      sb.Append("  Comment: ").Append(Comment).Append("\n");
      sb.Append("  InvoiceArticles: ").Append(InvoiceArticles).Append("\n");
      sb.Append("  CurrencyCode: ").Append(CurrencyCode).Append("\n");
      sb.Append("  TotalAmount: ").Append(TotalAmount).Append("\n");
      sb.Append("  InvoiceVatTotals: ").Append(InvoiceVatTotals).Append("\n");
      sb.Append("  TotalVatAmount: ").Append(TotalVatAmount).Append("\n");
      sb.Append("  TotalAmountExcludingVat: ").Append(TotalAmountExcludingVat).Append("\n");
      sb.Append("  MerchantId: ").Append(MerchantId).Append("\n");
      sb.Append("  InvoiceIssuerId: ").Append(InvoiceIssuerId).Append("\n");
      sb.Append("  InvoiceIssuerName: ").Append(InvoiceIssuerName).Append("\n");
      sb.Append("  InvoiceIssuerAddress: ").Append(InvoiceIssuerAddress).Append("\n");
      sb.Append("  InvoiceIssuerZipcode: ").Append(InvoiceIssuerZipcode).Append("\n");
      sb.Append("  InvoiceIssuerCity: ").Append(InvoiceIssuerCity).Append("\n");
      sb.Append("  MerchantIsoCountryCode: ").Append(MerchantIsoCountryCode).Append("\n");
      sb.Append("  LogoUrl: ").Append(LogoUrl).Append("\n");
      sb.Append("  Status: ").Append(Status).Append("\n");
      sb.Append("  InvoiceUrl: ").Append(InvoiceUrl).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
