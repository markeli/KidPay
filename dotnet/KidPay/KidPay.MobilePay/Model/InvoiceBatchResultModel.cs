using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class InvoiceBatchResultModel {
    /// <summary>
    /// Gets or Sets Accepted
    /// </summary>
    [DataMember(Name="Accepted", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "Accepted")]
    public List<InvoiceBatchResultModelItem> Accepted { get; set; }

    /// <summary>
    /// Gets or Sets Rejected
    /// </summary>
    [DataMember(Name="Rejected", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "Rejected")]
    public List<InvoiceBatchResultModelRejectemItem> Rejected { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class InvoiceBatchResultModel {\n");
      sb.Append("  Accepted: ").Append(Accepted).Append("\n");
      sb.Append("  Rejected: ").Append(Rejected).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
