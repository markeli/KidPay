using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class ApiKeyConfiguration {
    /// <summary>
    /// Gets or Sets ApiKey
    /// </summary>
    [DataMember(Name="ApiKey", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "ApiKey")]
    public string ApiKey { get; set; }

    /// <summary>
    /// Gets or Sets CallbackUrl
    /// </summary>
    [DataMember(Name="CallbackUrl", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "CallbackUrl")]
    public string CallbackUrl { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class ApiKeyConfiguration {\n");
      sb.Append("  ApiKey: ").Append(ApiKey).Append("\n");
      sb.Append("  CallbackUrl: ").Append(CallbackUrl).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
