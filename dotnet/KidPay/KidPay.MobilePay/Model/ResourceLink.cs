using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class ResourceLink {
    /// <summary>
    /// Gets or Sets Rel
    /// </summary>
    [DataMember(Name="Rel", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "Rel")]
    public string Rel { get; set; }

    /// <summary>
    /// Gets or Sets Href
    /// </summary>
    [DataMember(Name="Href", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "Href")]
    public string Href { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class ResourceLink {\n");
      sb.Append("  Rel: ").Append(Rel).Append("\n");
      sb.Append("  Href: ").Append(Href).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
