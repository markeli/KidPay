using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class InvoiceLinkInputModel {
    /// <summary>
    /// Gets or Sets InvoiceIssuer
    /// </summary>
    [DataMember(Name="InvoiceIssuer", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "InvoiceIssuer")]
    public Guid? InvoiceIssuer { get; set; }

    /// <summary>
    /// Gets or Sets ConsumerAlias
    /// </summary>
    [DataMember(Name="ConsumerAlias", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "ConsumerAlias")]
    public ConsumerAliasModel ConsumerAlias { get; set; }

    /// <summary>
    /// Gets or Sets ConsumerName
    /// </summary>
    [DataMember(Name="ConsumerName", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "ConsumerName")]
    public string ConsumerName { get; set; }

    /// <summary>
    /// Gets or Sets TotalAmount
    /// </summary>
    [DataMember(Name="TotalAmount", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "TotalAmount")]
    public double? TotalAmount { get; set; }

    /// <summary>
    /// Gets or Sets TotalVATAmount
    /// </summary>
    [DataMember(Name="TotalVATAmount", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "TotalVATAmount")]
    public double? TotalVATAmount { get; set; }

    /// <summary>
    /// Gets or Sets CountryCode
    /// </summary>
    [DataMember(Name="CountryCode", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "CountryCode")]
    public string CountryCode { get; set; }

    /// <summary>
    /// Gets or Sets CurrencyCode
    /// </summary>
    [DataMember(Name="CurrencyCode", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "CurrencyCode")]
    public string CurrencyCode { get; set; }

    /// <summary>
    /// Gets or Sets ConsumerAddressLines
    /// </summary>
    [DataMember(Name="ConsumerAddressLines", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "ConsumerAddressLines")]
    public List<string> ConsumerAddressLines { get; set; }

    /// <summary>
    /// Gets or Sets DeliveryAddressLines
    /// </summary>
    [DataMember(Name="DeliveryAddressLines", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "DeliveryAddressLines")]
    public List<string> DeliveryAddressLines { get; set; }

    /// <summary>
    /// Gets or Sets InvoiceNumber
    /// </summary>
    [DataMember(Name="InvoiceNumber", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "InvoiceNumber")]
    public string InvoiceNumber { get; set; }

    /// <summary>
    /// Gets or Sets IssueDate
    /// </summary>
    [DataMember(Name="IssueDate", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "IssueDate")]
    public DateTime? IssueDate { get; set; }

    /// <summary>
    /// Gets or Sets DueDate
    /// </summary>
    [DataMember(Name="DueDate", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "DueDate")]
    public DateTime? DueDate { get; set; }

    /// <summary>
    /// Gets or Sets OrderDate
    /// </summary>
    [DataMember(Name="OrderDate", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "OrderDate")]
    public DateTime? OrderDate { get; set; }

    /// <summary>
    /// Gets or Sets DeliveryDate
    /// </summary>
    [DataMember(Name="DeliveryDate", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "DeliveryDate")]
    public DateTime? DeliveryDate { get; set; }

    /// <summary>
    /// Gets or Sets Comment
    /// </summary>
    [DataMember(Name="Comment", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "Comment")]
    public string Comment { get; set; }

    /// <summary>
    /// Gets or Sets MerchantContactName
    /// </summary>
    [DataMember(Name="MerchantContactName", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "MerchantContactName")]
    public string MerchantContactName { get; set; }

    /// <summary>
    /// Gets or Sets MerchantOrderNumber
    /// </summary>
    [DataMember(Name="MerchantOrderNumber", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "MerchantOrderNumber")]
    public string MerchantOrderNumber { get; set; }

    /// <summary>
    /// Gets or Sets BuyerOrderNumber
    /// </summary>
    [DataMember(Name="BuyerOrderNumber", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "BuyerOrderNumber")]
    public string BuyerOrderNumber { get; set; }

    /// <summary>
    /// Gets or Sets PaymentReference
    /// </summary>
    [DataMember(Name="PaymentReference", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "PaymentReference")]
    public string PaymentReference { get; set; }

    /// <summary>
    /// Gets or Sets InvoiceArticles
    /// </summary>
    [DataMember(Name="InvoiceArticles", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "InvoiceArticles")]
    public List<InvoiceArticleModel> InvoiceArticles { get; set; }

    /// <summary>
    /// Gets or Sets InvoiceUrl
    /// </summary>
    [DataMember(Name="InvoiceUrl", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "InvoiceUrl")]
    public string InvoiceUrl { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class InvoiceLinkInputModel {\n");
      sb.Append("  InvoiceIssuer: ").Append(InvoiceIssuer).Append("\n");
      sb.Append("  ConsumerAlias: ").Append(ConsumerAlias).Append("\n");
      sb.Append("  ConsumerName: ").Append(ConsumerName).Append("\n");
      sb.Append("  TotalAmount: ").Append(TotalAmount).Append("\n");
      sb.Append("  TotalVATAmount: ").Append(TotalVATAmount).Append("\n");
      sb.Append("  CountryCode: ").Append(CountryCode).Append("\n");
      sb.Append("  CurrencyCode: ").Append(CurrencyCode).Append("\n");
      sb.Append("  ConsumerAddressLines: ").Append(ConsumerAddressLines).Append("\n");
      sb.Append("  DeliveryAddressLines: ").Append(DeliveryAddressLines).Append("\n");
      sb.Append("  InvoiceNumber: ").Append(InvoiceNumber).Append("\n");
      sb.Append("  IssueDate: ").Append(IssueDate).Append("\n");
      sb.Append("  DueDate: ").Append(DueDate).Append("\n");
      sb.Append("  OrderDate: ").Append(OrderDate).Append("\n");
      sb.Append("  DeliveryDate: ").Append(DeliveryDate).Append("\n");
      sb.Append("  Comment: ").Append(Comment).Append("\n");
      sb.Append("  MerchantContactName: ").Append(MerchantContactName).Append("\n");
      sb.Append("  MerchantOrderNumber: ").Append(MerchantOrderNumber).Append("\n");
      sb.Append("  BuyerOrderNumber: ").Append(BuyerOrderNumber).Append("\n");
      sb.Append("  PaymentReference: ").Append(PaymentReference).Append("\n");
      sb.Append("  InvoiceArticles: ").Append(InvoiceArticles).Append("\n");
      sb.Append("  InvoiceUrl: ").Append(InvoiceUrl).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
