using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class InvoiceIssuersList {
    /// <summary>
    /// Gets or Sets InvoiceIssuers
    /// </summary>
    [DataMember(Name="InvoiceIssuers", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "InvoiceIssuers")]
    public List<InvoiceIssuer> InvoiceIssuers { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class InvoiceIssuersList {\n");
      sb.Append("  InvoiceIssuers: ").Append(InvoiceIssuers).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
