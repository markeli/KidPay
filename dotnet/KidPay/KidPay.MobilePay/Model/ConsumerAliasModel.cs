using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class ConsumerAliasModel {
    /// <summary>
    /// Gets or Sets Alias
    /// </summary>
    [DataMember(Name="Alias", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "Alias")]
    public string Alias { get; set; }

    /// <summary>
    /// Gets or Sets AliasType
    /// </summary>
    [DataMember(Name="AliasType", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "AliasType")]
    public string AliasType { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class ConsumerAliasModel {\n");
      sb.Append("  Alias: ").Append(Alias).Append("\n");
      sb.Append("  AliasType: ").Append(AliasType).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
