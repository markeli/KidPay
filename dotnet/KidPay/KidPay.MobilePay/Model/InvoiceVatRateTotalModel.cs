using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class InvoiceVatRateTotalModel {
    /// <summary>
    /// Gets or Sets VatRate
    /// </summary>
    [DataMember(Name="VatRate", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "VatRate")]
    public double? VatRate { get; set; }

    /// <summary>
    /// Gets or Sets TotalVatAmount
    /// </summary>
    [DataMember(Name="TotalVatAmount", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "TotalVatAmount")]
    public double? TotalVatAmount { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class InvoiceVatRateTotalModel {\n");
      sb.Append("  VatRate: ").Append(VatRate).Append("\n");
      sb.Append("  TotalVatAmount: ").Append(TotalVatAmount).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
