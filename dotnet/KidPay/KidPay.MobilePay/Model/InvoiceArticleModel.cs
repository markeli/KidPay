using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class InvoiceArticleModel {
    /// <summary>
    /// Gets or Sets ArticleNumber
    /// </summary>
    [DataMember(Name="ArticleNumber", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "ArticleNumber")]
    public string ArticleNumber { get; set; }

    /// <summary>
    /// Gets or Sets ArticleDescription
    /// </summary>
    [DataMember(Name="ArticleDescription", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "ArticleDescription")]
    public string ArticleDescription { get; set; }

    /// <summary>
    /// Gets or Sets VATRate
    /// </summary>
    [DataMember(Name="VATRate", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "VATRate")]
    public double? VATRate { get; set; }

    /// <summary>
    /// Gets or Sets TotalVATAmount
    /// </summary>
    [DataMember(Name="TotalVATAmount", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "TotalVATAmount")]
    public double? TotalVATAmount { get; set; }

    /// <summary>
    /// Gets or Sets TotalPriceIncludingVat
    /// </summary>
    [DataMember(Name="TotalPriceIncludingVat", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "TotalPriceIncludingVat")]
    public double? TotalPriceIncludingVat { get; set; }

    /// <summary>
    /// Gets or Sets Unit
    /// </summary>
    [DataMember(Name="Unit", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "Unit")]
    public string Unit { get; set; }

    /// <summary>
    /// Gets or Sets Quantity
    /// </summary>
    [DataMember(Name="Quantity", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "Quantity")]
    public double? Quantity { get; set; }

    /// <summary>
    /// Gets or Sets PricePerUnit
    /// </summary>
    [DataMember(Name="PricePerUnit", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "PricePerUnit")]
    public double? PricePerUnit { get; set; }

    /// <summary>
    /// Gets or Sets PriceReduction
    /// </summary>
    [DataMember(Name="PriceReduction", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "PriceReduction")]
    public double? PriceReduction { get; set; }

    /// <summary>
    /// Gets or Sets PriceDiscount
    /// </summary>
    [DataMember(Name="PriceDiscount", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "PriceDiscount")]
    public double? PriceDiscount { get; set; }

    /// <summary>
    /// Gets or Sets Bonus
    /// </summary>
    [DataMember(Name="Bonus", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "Bonus")]
    public double? Bonus { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class InvoiceArticleModel {\n");
      sb.Append("  ArticleNumber: ").Append(ArticleNumber).Append("\n");
      sb.Append("  ArticleDescription: ").Append(ArticleDescription).Append("\n");
      sb.Append("  VATRate: ").Append(VATRate).Append("\n");
      sb.Append("  TotalVATAmount: ").Append(TotalVATAmount).Append("\n");
      sb.Append("  TotalPriceIncludingVat: ").Append(TotalPriceIncludingVat).Append("\n");
      sb.Append("  Unit: ").Append(Unit).Append("\n");
      sb.Append("  Quantity: ").Append(Quantity).Append("\n");
      sb.Append("  PricePerUnit: ").Append(PricePerUnit).Append("\n");
      sb.Append("  PriceReduction: ").Append(PriceReduction).Append("\n");
      sb.Append("  PriceDiscount: ").Append(PriceDiscount).Append("\n");
      sb.Append("  Bonus: ").Append(Bonus).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
