﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;
using Newtonsoft.Json;

namespace KidPay.MobilePay
{
    /// <inheritdoc />
    public class MobilePayProxy : IMobilePayProxy
    {
        private readonly MobilePaySettings _settings;
        private readonly HttpClient _httpClient;

        /// <inheritdoc />
        public MobilePayProxy(MobilePaySettings settings)
        {
            _settings = settings;

            _httpClient = new HttpClient();
            _httpClient.SetBearerToken(_settings.AccessToken);
        }


        /// <inheritdoc />
        public async Task<Guid> ProcessInvoiceAsync()
        {
            var httpRequest = new HttpRequestMessage(
                HttpMethod.Post, 
                $"{_settings.MobileAppApiBaseUrl}/api/v1/merchants/{_settings.MerchantId}/invoices");
                
                
            httpRequest.Headers.Add("Accept", "application/json");
            httpRequest.Headers.Add("x-ibm-client-secret", _settings.ClientSecret);
            httpRequest.Headers.Add("x-ibm-client-id", _settings.ClientId);
            var invoice = new InvoiceInputModel
            {
                Comment = "Any comment",
                ConsumerAddressLines = new List<string>{
                    "Paradisæblevej 13",
                    "CC-1234 Andeby", 
                    "WONDERLAND"},
                ConsumerAlias = new ConsumerAliasModel
                {
                    Alias = "+4577007700",
                    AliasType = "Phone"
                },
                ConsumerName = "Consumer Name", 
                TotalAmount = 360,
                TotalVATAmount = 72,
                CountryCode ="DK",
                CurrencyCode = "DKK",
                InvoiceIssuer = Guid.Parse(_settings.InvoiceIssuer),
                DeliveryAddressLines = new List<string>
                {
                    "Østerbrogade 120",
                    "CC-1234 Andeby",
                    "WONDERLAND"
                },
                InvoiceNumber = "301",
                IssueDate = DateTime.Now,
                DueDate = DateTime.Now.AddDays(3),
                OrderDate = DateTime.Now,
                DeliveryDate = DateTime.Now.AddDays(1),
                MerchantContactName = "Snowboard gear shop",
                MerchantOrderNumber = "938",
                BuyerOrderNumber = "631",
                PaymentReference = "186",
                InvoiceArticles = new List<InvoiceArticleModel>
                {
                    new InvoiceArticleModel
                    {
                        ArticleNumber = "1-123",
                        ArticleDescription = "Process Flying V Snowboard",
                        VATRate = 25,
                        TotalVATAmount = 72,
                        TotalPriceIncludingVat = 360,
                        Unit = "1",
                        Quantity = 1,
                        PricePerUnit = 288,
                        PriceReduction = 0,
                        PriceDiscount = 0,
                        Bonus = 0
                    }
                }
            };
            httpRequest.Content = 
                new StringContent(
                    JsonConvert.SerializeObject(invoice),
                    Encoding.UTF8,
                    "application/json");

            var responseMessage = await _httpClient.SendAsync(httpRequest);

            responseMessage.EnsureSuccessStatusCode();
            var content = await responseMessage.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<InvoiceResultModel>(content);
            
            return result.InvoiceId.Value;
        }

        /// <inheritdoc />
        public async Task<InvoiceStatusResultModel> GetStatusAsync(Guid invoiceId)
        {
            var apiClient = new ApiClient(_settings.MobileAppApiBaseUrl);

            var api = new DefaultApi(apiClient);

            var result = await Task.Factory.StartNew(() => api.ApiV1MerchantsMerchantidInvoicesInvoiceidStatusGet(
                Guid.Parse(_settings.MerchantId),
                invoiceId, 
                _settings.AccessToken));

            return result;
        }

        /// <inheritdoc />
        public Task AcceptInvoiceByCustomerAsync()
        {
            return Task.CompletedTask;
        }

        /// <inheritdoc />
        public Task RejectInvoiceByCustomerAsync()
        {
            return Task.CompletedTask;
        }

    }
}