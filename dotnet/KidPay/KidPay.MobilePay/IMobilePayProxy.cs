using System;
using System.Threading.Tasks;
using IdentityModel.OidcClient;
using IO.Swagger.Model;
using Newtonsoft.Json.Serialization;

namespace KidPay.MobilePay
{
    /// <summary>
    /// Provide access to MobilePay backend
    /// </summary>
    public interface IMobilePayProxy
    {
        /// <summary>
        /// Start processing new invoice from customer
        /// </summary>
        /// <returns></returns>
        Task<Guid> ProcessInvoiceAsync();

        /// <summary>
        /// Returns status of specified invoice
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        Task<InvoiceStatusResultModel> GetStatusAsync(Guid invoiceId);

        /// <summary>
        /// Accept invoice by customer
        /// </summary>
        /// <returns></returns>
        Task AcceptInvoiceByCustomerAsync();

        /// <summary>
        /// Reject invoice by customer
        /// </summary>
        /// <returns></returns>
        Task RejectInvoiceByCustomerAsync();
    }
}