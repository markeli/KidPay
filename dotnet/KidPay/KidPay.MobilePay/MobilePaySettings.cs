namespace KidPay.MobilePay
{
    /// <summary>
    /// Required for <see cref="IMobilePayProxy"/> configuration
    /// </summary>
    public class MobilePaySettings
    {
        /// <summary>
        /// OpenId authentication URL
        /// </summary>
        public string AuthUrl { get; set; }
        
        public string MerchantId { get; set; }
        
        public string ClientSecret { get; set; }
        
        public string ClientId { get; set; }
        
        public string MobileAppApiBaseUrl { get; set; }
        
        public string RedirectUri { get; set; }
        
        public string AccessToken { get; set; }
        
        public string RefreshToken { get; set; }
        
        public string InvoiceIssuer { get; set; }
    }
}