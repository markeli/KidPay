using System;
using System.Collections.Generic;
using RestSharp;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace IO.Swagger.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IMerchantsApi
    {
        /// <summary>
        /// Get all the details of an invoice. 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="invoiceId"></param>
        /// <param name="authenticatedUser">Id for user performing action (Must be a valid GUID)</param>
        /// <param name="correlationId">CorrelationId used for logging</param>
        /// <returns>InvoiceDetailsModel</returns>
        InvoiceDetailsModel MerchantsGetInvoice (Guid? merchantId, Guid? invoiceId, string authenticatedUser, string correlationId);
    }
  
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public class MerchantsApi : IMerchantsApi
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MerchantsApi"/> class.
        /// </summary>
        /// <param name="apiClient"> an instance of ApiClient (optional)</param>
        /// <returns></returns>
        public MerchantsApi(ApiClient apiClient = null)
        {
            if (apiClient == null) // use the default one in Configuration
                this.ApiClient = Configuration.DefaultApiClient; 
            else
                this.ApiClient = apiClient;
        }
    
        /// <summary>
        /// Initializes a new instance of the <see cref="MerchantsApi"/> class.
        /// </summary>
        /// <returns></returns>
        public MerchantsApi(String basePath)
        {
            this.ApiClient = new ApiClient(basePath);
        }
    
        /// <summary>
        /// Sets the base path of the API client.
        /// </summary>
        /// <param name="basePath">The base path</param>
        /// <value>The base path</value>
        public void SetBasePath(String basePath)
        {
            this.ApiClient.BasePath = basePath;
        }
    
        /// <summary>
        /// Gets the base path of the API client.
        /// </summary>
        /// <param name="basePath">The base path</param>
        /// <value>The base path</value>
        public String GetBasePath(String basePath)
        {
            return this.ApiClient.BasePath;
        }
    
        /// <summary>
        /// Gets or sets the API client.
        /// </summary>
        /// <value>An instance of the ApiClient</value>
        public ApiClient ApiClient {get; set;}
    
        /// <summary>
        /// Get all the details of an invoice. 
        /// </summary>
        /// <param name="merchantId"></param> 
        /// <param name="invoiceId"></param> 
        /// <param name="authenticatedUser">Id for user performing action (Must be a valid GUID)</param> 
        /// <param name="correlationId">CorrelationId used for logging</param> 
        /// <returns>InvoiceDetailsModel</returns>            
        public InvoiceDetailsModel MerchantsGetInvoice (Guid? merchantId, Guid? invoiceId, string authenticatedUser, string correlationId)
        {
            
            // verify the required parameter 'merchantId' is set
            if (merchantId == null) throw new ApiException(400, "Missing required parameter 'merchantId' when calling MerchantsGetInvoice");
            
            // verify the required parameter 'invoiceId' is set
            if (invoiceId == null) throw new ApiException(400, "Missing required parameter 'invoiceId' when calling MerchantsGetInvoice");
            
            // verify the required parameter 'authenticatedUser' is set
            if (authenticatedUser == null) throw new ApiException(400, "Missing required parameter 'authenticatedUser' when calling MerchantsGetInvoice");
            
    
            var path = "/api/v1/merchants/{merchantId}/invoices/{invoiceId}";
            path = path.Replace("{format}", "json");
            path = path.Replace("{" + "merchantId" + "}", ApiClient.ParameterToString(merchantId));
path = path.Replace("{" + "invoiceId" + "}", ApiClient.ParameterToString(invoiceId));
    
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
                         if (correlationId != null) headerParams.Add("CorrelationId", ApiClient.ParameterToString(correlationId)); // header parameter
 if (authenticatedUser != null) headerParams.Add("AuthenticatedUser", ApiClient.ParameterToString(authenticatedUser)); // header parameter
                            
            // authentication setting, if any
            String[] authSettings = new String[] { "ClientId", "ClientSecret" };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.GET, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling MerchantsGetInvoice: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling MerchantsGetInvoice: " + response.ErrorMessage, response.ErrorMessage);
    
            return (InvoiceDetailsModel) ApiClient.Deserialize(response.Content, typeof(InvoiceDetailsModel), response.Headers);
        }
    
    }
}
