using System;
using System.Collections.Generic;
using RestSharp;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace IO.Swagger.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IDefaultApi
    {
        /// <summary>
        /// Get merchant id ## How to use this call? **MerchantId** is a unique identifier of a merchant in our system. After you retrieve an access token from OpenID flow, then use the following endpoint to retrieve your **MerchantId**.  Read more on GitHub [here](https://mobilepaydev.github.io/MobilePay-Invoice/invoice_issuers)
        /// </summary>
        /// <param name="authorization">Bearer token</param>
        /// <returns>MerchantInfo</returns>
        MerchantInfo ApiV1MerchantsMeGet (string authorization);
        /// <summary>
        /// Change REST callback authentication scheme to API Key. All the REST callbacks will be sent to &#x60;CallbackUrl&#x60; and provided API key will be added to &#x60;Authorization&#x60; HTTP  header.  &lt;para /&gt;  More information about callbacks can be found [here](https://mobilepaydev.github.io/MobilePay-Invoice/callbacks).
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="input"></param>
        /// <param name="authorization">Bearer token</param>
        /// <returns></returns>
        void ApiV1MerchantsMerchantIdAuthApikeyPut (Guid? merchantId, ApiKeyConfiguration input, string authorization);
        /// <summary>
        /// Change REST callback authentication scheme to basic. All the REST callbacks will be sent to &#x60;CallbackUrl&#x60; and contain basic credentials in &#x60;Authorization&#x60; HTTP header:  &#x60;Authorization: Basic [Base64 encoded username:password]&#x60;  &lt;para /&gt;  More information about callbacks can be found [here](https://mobilepaydev.github.io/MobilePay-Invoice/callbacks).
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="input"></param>
        /// <param name="authorization">Bearer token</param>
        /// <returns></returns>
        void ApiV1MerchantsMerchantIdAuthBasicPut (Guid? merchantId, BasicAuthenticationConfiguration input, string authorization);
        /// <summary>
        /// Create multiple InvoiceDirect. &#x60;ConsumerAlias.AliasType&#x60; should be set to \&quot;Phone\&quot;.  &#x60;CountryCode&#x60; allowed values:  * DK  * FI   &#x60;CurrencyCode&#x60; allowed values:  * DKK  * EUR  More information can be found for creating multiple InvoiceDirect [here](https://mobilepaydev.github.io/MobilePay-Invoice/api_reference#direct).
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="invoiceInputModels"></param>
        /// <param name="authorization">Bearer token</param>
        /// <returns>InvoiceBatchResultModel</returns>
        InvoiceBatchResultModel ApiV1MerchantsMerchantIdInvoicesBatchPost (Guid? merchantId, List<InvoiceInputModel> invoiceInputModels, string authorization);
        /// <summary>
        /// Cancel an unpaid invoice. 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="invoiceId"></param>
        /// <param name="authorization">Bearer token</param>
        /// <returns></returns>
        void ApiV1MerchantsMerchantIdInvoicesInvoiceIdCancelPut (Guid? merchantId, Guid? invoiceId, string authorization);
        /// <summary>
        /// Create multiple [InvoiceLink](https://mobilepaydev.github.io/MobilePay-Invoice/#invoice-link). &#x60;ConsumerAlias.AliasType&#x60; should be set to \&quot;Phone\&quot;.  &#x60;CountryCode&#x60; allowed values:  * DK  * FI   &#x60;CurrencyCode&#x60; allowed values:  * DKK  * EUR  More information can be found [here](https://mobilepaydev.github.io/MobilePay-Invoice/#batch-requests).
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="invoiceInputModels"></param>
        /// <param name="authorization">Bearer token</param>
        /// <returns>InvoiceBatchResultModel</returns>
        InvoiceBatchResultModel ApiV1MerchantsMerchantIdInvoicesLinkBatchPost (Guid? merchantId, List<InvoiceLinkInputModel> invoiceInputModels, string authorization);
        /// <summary>
        /// Create an [InvoiceLink](https://mobilepaydev.github.io/MobilePay-Invoice/#invoice-link). ## How to use this call?  Use this call to create a Link to Invoice, that is sent to the. To view the invoice, the customer clicks the invoice link \&quot;Pay with MobilePay\&quot; button.  Note: The request does not require a ConsumerAlias because any MobilePay user can pay InvoiceLink securely via MobilePay. The customer can share the MobilePay InvoiceLink.   Not all parameters are required. Find the full parameter for InvoiceLink description on GitHub [here](https://mobilepaydev.github.io/MobilePay-Invoice/api_reference#link)   &#x60;ConsumerAlias.AliasType&#x60; should be set to \&quot;Phone\&quot;.  &#x60;CountryCode&#x60; allowed values:  * DK  * FI   &#x60;CurrencyCode&#x60; allowed values:  * DKK  * EUR
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="input"></param>
        /// <param name="authorization">Bearer token</param>
        /// <returns>InvoiceLinkResultModel</returns>
        InvoiceLinkResultModel ApiV1MerchantsMerchantIdInvoicesLinkPost (Guid? merchantId, InvoiceLinkInputModel input, string authorization);
        /// <summary>
        /// Get all your invoice issuers. 
        /// </summary>
        /// <param name="merchantid"></param>
        /// <param name="authorization">Bearer token</param>
        /// <returns>InvoiceIssuersList</returns>
        InvoiceIssuersList ApiV1MerchantsMerchantidInvoiceissuersGet (Guid? merchantid, string authorization);
        /// <summary>
        /// Get the current status of an invoice. Possible statuses:  * created  * invalid  * accepted  * paid  * rejected  * expired  Explanations for these statuses can be found [here](https://mobilepaydev.github.io/MobilePay-Invoice/api_reference#get-status).
        /// </summary>
        /// <param name="merchantid"></param>
        /// <param name="invoiceid"></param>
        /// <param name="authorization">Bearer token</param>
        /// <returns>InvoiceStatusResultModel</returns>
        InvoiceStatusResultModel ApiV1MerchantsMerchantidInvoicesInvoiceidStatusGet (Guid? merchantid, Guid? invoiceid, string authorization);
        /// <summary>
        /// Create an [InvoiceDirect](https://mobilepaydev.github.io/MobilePay-Invoice/api_reference#direct). ## How to use this call?  Use this call so that the customer sees a MobilePay notification. The customer sees the Invoice in Activities in the app. The customer clicks the Invoice and sees the content of the invoice, and clicks to continue to pay, and then chooses the preferred pay date and swipe.  Not all parameters are required. Find the full parameter for InvoiceDirect description on GitHub [here](https://mobilepaydev.github.io/MobilePay-Invoice/api_reference#direct)  &#x60;ConsumerAlias.AliasType&#x60; should be set to \&quot;Phone\&quot;.  &#x60;CountryCode&#x60; allowed values:  * DK  * FI   &#x60;CurrencyCode&#x60; allowed values:  * DKK  * EUR
        /// </summary>
        /// <param name="merchantid"></param>
        /// <param name="input"></param>
        /// <param name="authorization">Bearer token</param>
        /// <returns>InvoiceResultModel</returns>
        InvoiceResultModel ApiV1MerchantsMerchantidInvoicesPost (Guid? merchantid, InvoiceInputModel input, string authorization);
    }
  
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public class DefaultApi : IDefaultApi
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultApi"/> class.
        /// </summary>
        /// <param name="apiClient"> an instance of ApiClient (optional)</param>
        /// <returns></returns>
        public DefaultApi(ApiClient apiClient = null)
        {
            if (apiClient == null) // use the default one in Configuration
                this.ApiClient = Configuration.DefaultApiClient; 
            else
                this.ApiClient = apiClient;
        }
    
        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultApi"/> class.
        /// </summary>
        /// <returns></returns>
        public DefaultApi(String basePath)
        {
            this.ApiClient = new ApiClient(basePath);
        }
    
        /// <summary>
        /// Sets the base path of the API client.
        /// </summary>
        /// <param name="basePath">The base path</param>
        /// <value>The base path</value>
        public void SetBasePath(String basePath)
        {
            this.ApiClient.BasePath = basePath;
        }
    
        /// <summary>
        /// Gets the base path of the API client.
        /// </summary>
        /// <param name="basePath">The base path</param>
        /// <value>The base path</value>
        public String GetBasePath(String basePath)
        {
            return this.ApiClient.BasePath;
        }
    
        /// <summary>
        /// Gets or sets the API client.
        /// </summary>
        /// <value>An instance of the ApiClient</value>
        public ApiClient ApiClient {get; set;}
    
        /// <summary>
        /// Get merchant id ## How to use this call? **MerchantId** is a unique identifier of a merchant in our system. After you retrieve an access token from OpenID flow, then use the following endpoint to retrieve your **MerchantId**.  Read more on GitHub [here](https://mobilepaydev.github.io/MobilePay-Invoice/invoice_issuers)
        /// </summary>
        /// <param name="authorization">Bearer token</param> 
        /// <returns>MerchantInfo</returns>            
        public MerchantInfo ApiV1MerchantsMeGet (string authorization)
        {
            
            // verify the required parameter 'authorization' is set
            if (authorization == null) throw new ApiException(400, "Missing required parameter 'authorization' when calling ApiV1MerchantsMeGet");
            
    
            var path = "/api/v1/merchants/me";
            path = path.Replace("{format}", "json");
                
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
                         if (authorization != null) headerParams.Add("Authorization", ApiClient.ParameterToString(authorization)); // header parameter
                            
            // authentication setting, if any
            String[] authSettings = new String[] { "ClientId", "ClientSecret" };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.GET, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiV1MerchantsMeGet: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiV1MerchantsMeGet: " + response.ErrorMessage, response.ErrorMessage);
    
            return (MerchantInfo) ApiClient.Deserialize(response.Content, typeof(MerchantInfo), response.Headers);
        }
    
        /// <summary>
        /// Change REST callback authentication scheme to API Key. All the REST callbacks will be sent to &#x60;CallbackUrl&#x60; and provided API key will be added to &#x60;Authorization&#x60; HTTP  header.  &lt;para /&gt;  More information about callbacks can be found [here](https://mobilepaydev.github.io/MobilePay-Invoice/callbacks).
        /// </summary>
        /// <param name="merchantId"></param> 
        /// <param name="input"></param> 
        /// <param name="authorization">Bearer token</param> 
        /// <returns></returns>            
        public void ApiV1MerchantsMerchantIdAuthApikeyPut (Guid? merchantId, ApiKeyConfiguration input, string authorization)
        {
            
            // verify the required parameter 'merchantId' is set
            if (merchantId == null) throw new ApiException(400, "Missing required parameter 'merchantId' when calling ApiV1MerchantsMerchantIdAuthApikeyPut");
            
            // verify the required parameter 'input' is set
            if (input == null) throw new ApiException(400, "Missing required parameter 'input' when calling ApiV1MerchantsMerchantIdAuthApikeyPut");
            
            // verify the required parameter 'authorization' is set
            if (authorization == null) throw new ApiException(400, "Missing required parameter 'authorization' when calling ApiV1MerchantsMerchantIdAuthApikeyPut");
            
    
            var path = "/api/v1/merchants/{merchantId}/auth/apikey";
            path = path.Replace("{format}", "json");
            path = path.Replace("{" + "merchantId" + "}", ApiClient.ParameterToString(merchantId));
    
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
                         if (authorization != null) headerParams.Add("Authorization", ApiClient.ParameterToString(authorization)); // header parameter
                        postBody = ApiClient.Serialize(input); // http body (model) parameter
    
            // authentication setting, if any
            String[] authSettings = new String[] { "ClientId", "ClientSecret" };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.PUT, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiV1MerchantsMerchantIdAuthApikeyPut: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiV1MerchantsMerchantIdAuthApikeyPut: " + response.ErrorMessage, response.ErrorMessage);
    
            return;
        }
    
        /// <summary>
        /// Change REST callback authentication scheme to basic. All the REST callbacks will be sent to &#x60;CallbackUrl&#x60; and contain basic credentials in &#x60;Authorization&#x60; HTTP header:  &#x60;Authorization: Basic [Base64 encoded username:password]&#x60;  &lt;para /&gt;  More information about callbacks can be found [here](https://mobilepaydev.github.io/MobilePay-Invoice/callbacks).
        /// </summary>
        /// <param name="merchantId"></param> 
        /// <param name="input"></param> 
        /// <param name="authorization">Bearer token</param> 
        /// <returns></returns>            
        public void ApiV1MerchantsMerchantIdAuthBasicPut (Guid? merchantId, BasicAuthenticationConfiguration input, string authorization)
        {
            
            // verify the required parameter 'merchantId' is set
            if (merchantId == null) throw new ApiException(400, "Missing required parameter 'merchantId' when calling ApiV1MerchantsMerchantIdAuthBasicPut");
            
            // verify the required parameter 'input' is set
            if (input == null) throw new ApiException(400, "Missing required parameter 'input' when calling ApiV1MerchantsMerchantIdAuthBasicPut");
            
            // verify the required parameter 'authorization' is set
            if (authorization == null) throw new ApiException(400, "Missing required parameter 'authorization' when calling ApiV1MerchantsMerchantIdAuthBasicPut");
            
    
            var path = "/api/v1/merchants/{merchantId}/auth/basic";
            path = path.Replace("{format}", "json");
            path = path.Replace("{" + "merchantId" + "}", ApiClient.ParameterToString(merchantId));
    
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
                         if (authorization != null) headerParams.Add("Authorization", ApiClient.ParameterToString(authorization)); // header parameter
                        postBody = ApiClient.Serialize(input); // http body (model) parameter
    
            // authentication setting, if any
            String[] authSettings = new String[] { "ClientId", "ClientSecret" };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.PUT, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiV1MerchantsMerchantIdAuthBasicPut: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiV1MerchantsMerchantIdAuthBasicPut: " + response.ErrorMessage, response.ErrorMessage);
    
            return;
        }
    
        /// <summary>
        /// Create multiple InvoiceDirect. &#x60;ConsumerAlias.AliasType&#x60; should be set to \&quot;Phone\&quot;.  &#x60;CountryCode&#x60; allowed values:  * DK  * FI   &#x60;CurrencyCode&#x60; allowed values:  * DKK  * EUR  More information can be found for creating multiple InvoiceDirect [here](https://mobilepaydev.github.io/MobilePay-Invoice/api_reference#direct).
        /// </summary>
        /// <param name="merchantId"></param> 
        /// <param name="invoiceInputModels"></param> 
        /// <param name="authorization">Bearer token</param> 
        /// <returns>InvoiceBatchResultModel</returns>            
        public InvoiceBatchResultModel ApiV1MerchantsMerchantIdInvoicesBatchPost (Guid? merchantId, List<InvoiceInputModel> invoiceInputModels, string authorization)
        {
            
            // verify the required parameter 'merchantId' is set
            if (merchantId == null) throw new ApiException(400, "Missing required parameter 'merchantId' when calling ApiV1MerchantsMerchantIdInvoicesBatchPost");
            
            // verify the required parameter 'invoiceInputModels' is set
            if (invoiceInputModels == null) throw new ApiException(400, "Missing required parameter 'invoiceInputModels' when calling ApiV1MerchantsMerchantIdInvoicesBatchPost");
            
            // verify the required parameter 'authorization' is set
            if (authorization == null) throw new ApiException(400, "Missing required parameter 'authorization' when calling ApiV1MerchantsMerchantIdInvoicesBatchPost");
            
    
            var path = "/api/v1/merchants/{merchantId}/invoices/batch";
            path = path.Replace("{format}", "json");
            path = path.Replace("{" + "merchantId" + "}", ApiClient.ParameterToString(merchantId));
    
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
                         if (authorization != null) headerParams.Add("Authorization", ApiClient.ParameterToString(authorization)); // header parameter
                        postBody = ApiClient.Serialize(invoiceInputModels); // http body (model) parameter
    
            // authentication setting, if any
            String[] authSettings = new String[] { "ClientId", "ClientSecret" };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.POST, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiV1MerchantsMerchantIdInvoicesBatchPost: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiV1MerchantsMerchantIdInvoicesBatchPost: " + response.ErrorMessage, response.ErrorMessage);
    
            return (InvoiceBatchResultModel) ApiClient.Deserialize(response.Content, typeof(InvoiceBatchResultModel), response.Headers);
        }
    
        /// <summary>
        /// Cancel an unpaid invoice. 
        /// </summary>
        /// <param name="merchantId"></param> 
        /// <param name="invoiceId"></param> 
        /// <param name="authorization">Bearer token</param> 
        /// <returns></returns>            
        public void ApiV1MerchantsMerchantIdInvoicesInvoiceIdCancelPut (Guid? merchantId, Guid? invoiceId, string authorization)
        {
            
            // verify the required parameter 'merchantId' is set
            if (merchantId == null) throw new ApiException(400, "Missing required parameter 'merchantId' when calling ApiV1MerchantsMerchantIdInvoicesInvoiceIdCancelPut");
            
            // verify the required parameter 'invoiceId' is set
            if (invoiceId == null) throw new ApiException(400, "Missing required parameter 'invoiceId' when calling ApiV1MerchantsMerchantIdInvoicesInvoiceIdCancelPut");
            
            // verify the required parameter 'authorization' is set
            if (authorization == null) throw new ApiException(400, "Missing required parameter 'authorization' when calling ApiV1MerchantsMerchantIdInvoicesInvoiceIdCancelPut");
            
    
            var path = "/api/v1/merchants/{merchantId}/invoices/{invoiceId}/cancel";
            path = path.Replace("{format}", "json");
            path = path.Replace("{" + "merchantId" + "}", ApiClient.ParameterToString(merchantId));
path = path.Replace("{" + "invoiceId" + "}", ApiClient.ParameterToString(invoiceId));
    
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
                         if (authorization != null) headerParams.Add("Authorization", ApiClient.ParameterToString(authorization)); // header parameter
                            
            // authentication setting, if any
            String[] authSettings = new String[] { "ClientId", "ClientSecret" };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.PUT, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiV1MerchantsMerchantIdInvoicesInvoiceIdCancelPut: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiV1MerchantsMerchantIdInvoicesInvoiceIdCancelPut: " + response.ErrorMessage, response.ErrorMessage);
    
            return;
        }
    
        /// <summary>
        /// Create multiple [InvoiceLink](https://mobilepaydev.github.io/MobilePay-Invoice/#invoice-link). &#x60;ConsumerAlias.AliasType&#x60; should be set to \&quot;Phone\&quot;.  &#x60;CountryCode&#x60; allowed values:  * DK  * FI   &#x60;CurrencyCode&#x60; allowed values:  * DKK  * EUR  More information can be found [here](https://mobilepaydev.github.io/MobilePay-Invoice/#batch-requests).
        /// </summary>
        /// <param name="merchantId"></param> 
        /// <param name="invoiceInputModels"></param> 
        /// <param name="authorization">Bearer token</param> 
        /// <returns>InvoiceBatchResultModel</returns>            
        public InvoiceBatchResultModel ApiV1MerchantsMerchantIdInvoicesLinkBatchPost (Guid? merchantId, List<InvoiceLinkInputModel> invoiceInputModels, string authorization)
        {
            
            // verify the required parameter 'merchantId' is set
            if (merchantId == null) throw new ApiException(400, "Missing required parameter 'merchantId' when calling ApiV1MerchantsMerchantIdInvoicesLinkBatchPost");
            
            // verify the required parameter 'invoiceInputModels' is set
            if (invoiceInputModels == null) throw new ApiException(400, "Missing required parameter 'invoiceInputModels' when calling ApiV1MerchantsMerchantIdInvoicesLinkBatchPost");
            
            // verify the required parameter 'authorization' is set
            if (authorization == null) throw new ApiException(400, "Missing required parameter 'authorization' when calling ApiV1MerchantsMerchantIdInvoicesLinkBatchPost");
            
    
            var path = "/api/v1/merchants/{merchantId}/invoices/link/batch";
            path = path.Replace("{format}", "json");
            path = path.Replace("{" + "merchantId" + "}", ApiClient.ParameterToString(merchantId));
    
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
                         if (authorization != null) headerParams.Add("Authorization", ApiClient.ParameterToString(authorization)); // header parameter
                        postBody = ApiClient.Serialize(invoiceInputModels); // http body (model) parameter
    
            // authentication setting, if any
            String[] authSettings = new String[] { "ClientId", "ClientSecret" };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.POST, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiV1MerchantsMerchantIdInvoicesLinkBatchPost: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiV1MerchantsMerchantIdInvoicesLinkBatchPost: " + response.ErrorMessage, response.ErrorMessage);
    
            return (InvoiceBatchResultModel) ApiClient.Deserialize(response.Content, typeof(InvoiceBatchResultModel), response.Headers);
        }
    
        /// <summary>
        /// Create an [InvoiceLink](https://mobilepaydev.github.io/MobilePay-Invoice/#invoice-link). ## How to use this call?  Use this call to create a Link to Invoice, that is sent to the. To view the invoice, the customer clicks the invoice link \&quot;Pay with MobilePay\&quot; button.  Note: The request does not require a ConsumerAlias because any MobilePay user can pay InvoiceLink securely via MobilePay. The customer can share the MobilePay InvoiceLink.   Not all parameters are required. Find the full parameter for InvoiceLink description on GitHub [here](https://mobilepaydev.github.io/MobilePay-Invoice/api_reference#link)   &#x60;ConsumerAlias.AliasType&#x60; should be set to \&quot;Phone\&quot;.  &#x60;CountryCode&#x60; allowed values:  * DK  * FI   &#x60;CurrencyCode&#x60; allowed values:  * DKK  * EUR
        /// </summary>
        /// <param name="merchantId"></param> 
        /// <param name="input"></param> 
        /// <param name="authorization">Bearer token</param> 
        /// <returns>InvoiceLinkResultModel</returns>            
        public InvoiceLinkResultModel ApiV1MerchantsMerchantIdInvoicesLinkPost (Guid? merchantId, InvoiceLinkInputModel input, string authorization)
        {
            
            // verify the required parameter 'merchantId' is set
            if (merchantId == null) throw new ApiException(400, "Missing required parameter 'merchantId' when calling ApiV1MerchantsMerchantIdInvoicesLinkPost");
            
            // verify the required parameter 'input' is set
            if (input == null) throw new ApiException(400, "Missing required parameter 'input' when calling ApiV1MerchantsMerchantIdInvoicesLinkPost");
            
            // verify the required parameter 'authorization' is set
            if (authorization == null) throw new ApiException(400, "Missing required parameter 'authorization' when calling ApiV1MerchantsMerchantIdInvoicesLinkPost");
            
    
            var path = "/api/v1/merchants/{merchantId}/invoices/link";
            path = path.Replace("{format}", "json");
            path = path.Replace("{" + "merchantId" + "}", ApiClient.ParameterToString(merchantId));
    
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
                         if (authorization != null) headerParams.Add("Authorization", ApiClient.ParameterToString(authorization)); // header parameter
                        postBody = ApiClient.Serialize(input); // http body (model) parameter
    
            // authentication setting, if any
            String[] authSettings = new String[] { "ClientId", "ClientSecret" };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.POST, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiV1MerchantsMerchantIdInvoicesLinkPost: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiV1MerchantsMerchantIdInvoicesLinkPost: " + response.ErrorMessage, response.ErrorMessage);
    
            return (InvoiceLinkResultModel) ApiClient.Deserialize(response.Content, typeof(InvoiceLinkResultModel), response.Headers);
        }
    
        /// <summary>
        /// Get all your invoice issuers. 
        /// </summary>
        /// <param name="merchantid"></param> 
        /// <param name="authorization">Bearer token</param> 
        /// <returns>InvoiceIssuersList</returns>            
        public InvoiceIssuersList ApiV1MerchantsMerchantidInvoiceissuersGet (Guid? merchantid, string authorization)
        {
            
            // verify the required parameter 'merchantid' is set
            if (merchantid == null) throw new ApiException(400, "Missing required parameter 'merchantid' when calling ApiV1MerchantsMerchantidInvoiceissuersGet");
            
            // verify the required parameter 'authorization' is set
            if (authorization == null) throw new ApiException(400, "Missing required parameter 'authorization' when calling ApiV1MerchantsMerchantidInvoiceissuersGet");
            
    
            var path = "/api/v1/merchants/{merchantid}/invoiceissuers";
            path = path.Replace("{format}", "json");
            path = path.Replace("{" + "merchantid" + "}", ApiClient.ParameterToString(merchantid));
    
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
                         if (authorization != null) headerParams.Add("Authorization", ApiClient.ParameterToString(authorization)); // header parameter
                            
            // authentication setting, if any
            String[] authSettings = new String[] { "ClientId", "ClientSecret" };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.GET, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiV1MerchantsMerchantidInvoiceissuersGet: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiV1MerchantsMerchantidInvoiceissuersGet: " + response.ErrorMessage, response.ErrorMessage);
    
            return (InvoiceIssuersList) ApiClient.Deserialize(response.Content, typeof(InvoiceIssuersList), response.Headers);
        }
    
        /// <summary>
        /// Get the current status of an invoice. Possible statuses:  * created  * invalid  * accepted  * paid  * rejected  * expired  Explanations for these statuses can be found [here](https://mobilepaydev.github.io/MobilePay-Invoice/api_reference#get-status).
        /// </summary>
        /// <param name="merchantid"></param> 
        /// <param name="invoiceid"></param> 
        /// <param name="authorization">Bearer token</param> 
        /// <returns>InvoiceStatusResultModel</returns>            
        public InvoiceStatusResultModel ApiV1MerchantsMerchantidInvoicesInvoiceidStatusGet (Guid? merchantid, Guid? invoiceid, string authorization)
        {
            
            // verify the required parameter 'merchantid' is set
            if (merchantid == null) throw new ApiException(400, "Missing required parameter 'merchantid' when calling ApiV1MerchantsMerchantidInvoicesInvoiceidStatusGet");
            
            // verify the required parameter 'invoiceid' is set
            if (invoiceid == null) throw new ApiException(400, "Missing required parameter 'invoiceid' when calling ApiV1MerchantsMerchantidInvoicesInvoiceidStatusGet");
            
            // verify the required parameter 'authorization' is set
            if (authorization == null) throw new ApiException(400, "Missing required parameter 'authorization' when calling ApiV1MerchantsMerchantidInvoicesInvoiceidStatusGet");
            
    
            var path = "/api/v1/merchants/{merchantid}/invoices/{invoiceid}/status";
            path = path.Replace("{format}", "json");
            path = path.Replace("{" + "merchantid" + "}", ApiClient.ParameterToString(merchantid));
path = path.Replace("{" + "invoiceid" + "}", ApiClient.ParameterToString(invoiceid));
    
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
                         if (authorization != null) headerParams.Add("Authorization", ApiClient.ParameterToString(authorization)); // header parameter
                            
            // authentication setting, if any
            String[] authSettings = new String[] { "ClientId", "ClientSecret" };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.GET, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiV1MerchantsMerchantidInvoicesInvoiceidStatusGet: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiV1MerchantsMerchantidInvoicesInvoiceidStatusGet: " + response.ErrorMessage, response.ErrorMessage);
    
            return (InvoiceStatusResultModel) ApiClient.Deserialize(response.Content, typeof(InvoiceStatusResultModel), response.Headers);
        }
    
        /// <summary>
        /// Create an [InvoiceDirect](https://mobilepaydev.github.io/MobilePay-Invoice/api_reference#direct). ## How to use this call?  Use this call so that the customer sees a MobilePay notification. The customer sees the Invoice in Activities in the app. The customer clicks the Invoice and sees the content of the invoice, and clicks to continue to pay, and then chooses the preferred pay date and swipe.  Not all parameters are required. Find the full parameter for InvoiceDirect description on GitHub [here](https://mobilepaydev.github.io/MobilePay-Invoice/api_reference#direct)  &#x60;ConsumerAlias.AliasType&#x60; should be set to \&quot;Phone\&quot;.  &#x60;CountryCode&#x60; allowed values:  * DK  * FI   &#x60;CurrencyCode&#x60; allowed values:  * DKK  * EUR
        /// </summary>
        /// <param name="merchantid"></param> 
        /// <param name="input"></param> 
        /// <param name="authorization">Bearer token</param> 
        /// <returns>InvoiceResultModel</returns>            
        public InvoiceResultModel ApiV1MerchantsMerchantidInvoicesPost (Guid? merchantid, InvoiceInputModel input, string authorization)
        {
            
            // verify the required parameter 'merchantid' is set
            if (merchantid == null) throw new ApiException(400, "Missing required parameter 'merchantid' when calling ApiV1MerchantsMerchantidInvoicesPost");
            
            // verify the required parameter 'input' is set
            if (input == null) throw new ApiException(400, "Missing required parameter 'input' when calling ApiV1MerchantsMerchantidInvoicesPost");
            
            // verify the required parameter 'authorization' is set
            if (authorization == null) throw new ApiException(400, "Missing required parameter 'authorization' when calling ApiV1MerchantsMerchantidInvoicesPost");
            
    
            var path = "/api/v1/merchants/{merchantid}/invoices";
            path = path.Replace("{format}", "json");
            path = path.Replace("{" + "merchantid" + "}", ApiClient.ParameterToString(merchantid));
    
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
                         if (authorization != null) headerParams.Add("Authorization", ApiClient.ParameterToString(authorization)); // header parameter
                        postBody = ApiClient.Serialize(input); // http body (model) parameter
    
            // authentication setting, if any
            String[] authSettings = new String[] { "ClientId", "ClientSecret" };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.POST, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiV1MerchantsMerchantidInvoicesPost: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiV1MerchantsMerchantidInvoicesPost: " + response.ErrorMessage, response.ErrorMessage);
    
            return (InvoiceResultModel) ApiClient.Deserialize(response.Content, typeof(InvoiceResultModel), response.Headers);
        }
    
    }
}
